//
//  CalculatorInfo.m
//  
//
//  Created by Jeremiah on 6/26/18.
//
//



#import "CalculatorInfo.h"

@implementation CalculatorInfo

@synthesize title = _title;
@synthesize rvalue = _rvalue;


- (id)initWithTitle:(NSString*)title rvalue:(float)rvalue {
    if ((self = [super init])) {
        self.title = title;
        self.rvalue = rvalue;
    }
    return self;
}

@end