//
//  CalculatorInfo.h
//  
//
//  Created by Jeremiah on 6/26/18.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface CalculatorInfo : NSObject

@property (strong) NSString *title;
@property (assign) float rvalue;


- (id)initWithTitle:(NSString*)title rvalue:(float)rvalue;

@end