//
//  AmpacityTableView.m
//  Electrikit 0.1
//
//  Created by Jeremiah on 7/31/18.
//  Copyright (c) 2018 Jeremiah. All rights reserved.
//

#import "AmpacityTableView.h"

@interface AmpacityTableView ()



@end


@implementation AmpacityTableView

@synthesize AmpacityTableWV;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    AmpacityTableWV.delegate = self;
    [self.view addSubview:AmpacityTableWV];
    self.title = @"Ampacity Table";
    
    //Adjust webview (x, y , width, height)
    
    // [self.view setFrame:CGRectMake(0,-110,self.view.frame.size.width,self.view.frame.size.height)];
    // Setting PDF webview for the Ampacity table. 155 80
    UIWebView *webView = [[UIWebView alloc] initWithFrame:CGRectMake(self.view.center.x-((self.view.frame.size.width+125)/3), (self.view.center.y-(self.view.frame.size.height-245)/4), (self.view.frame.size.width+150)/1.5, (self.view.frame.size.height+170)/1.8)];
    
    NSURL *targetURL = [[NSBundle mainBundle] URLForResource:@"Ampacity_Table" withExtension:@"pdf"];
    NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
    [webView loadRequest:request];
    [self.view addSubview:webView];

    
    webView.scalesPageToFit=YES;
   
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
