//
//  AWGview.m
//  Electrikit 0.1
//
//  Created by Jeremiah on 6/27/18.
//  Copyright (c) 2018 Jeremiah. All rights reserved.
//

#import "AWGview.h"

#define PICKER_MIN 1
#define Picker_MAX 106
@interface AWGview (){
    
}

@end

@implementation AWGview
@synthesize inchesTF, inches2TF, mmTF, mm2TF,kcmTF, cuRTF, alRTF, curftTF, alrftTF, awgPK;
double awgNumber;



#pragma mark - Managing the detail item


- (void)configureView {
    // Update the user interface for the detail item.
}

//WireGauge drop down list
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    NSString * title = nil;
    

    //pickerView.transform = CGAffineTransformMakeScale(.7, 0.7);
    switch(row) {
        case 0:
            title = @"000000000";
            break;
        case 1:
            title = @"000000";
            break;
        case 2:
            title = @"0000";
            break;
        case 3:
            title = @"000";
            break;
        case 4:
            title = @"00";
            break;
        case 5:
            title = @"0";
            break;
        default:
            title =  [NSString stringWithFormat:@"%ld", (row+PICKER_MIN - 6)];
            
    }
    return title;
    
    
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return (Picker_MAX-PICKER_MIN+1);
}




//Getting values from picker
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    switch(row) {
        case 0:
            awgNumber = 1 - 6;
            break;
        case 1:
            awgNumber = 1 - 5;
            break;
        case 2:
            awgNumber = 1 - 4;
            break;
        case 3:
            awgNumber = 1 - 3;
            break;
        case 4:
            awgNumber = 1 - 2;
            break;
        case 5:
            awgNumber = 1 - 1;
            break;
        default:
            awgNumber = (row - 5);
    }
    
    
    
  
    //calculations by formula
    double inc, mm, mmTwo, incTwo, kcm, cuR, alR, cuRFT, alRFT;
    inc = 0.005 * pow(92,(36 - awgNumber) / 39);
    mm = 0.127 * pow(92,(36 - awgNumber) / 39);
    mmTwo = 0.012668 * pow(92,(36 - awgNumber) / 19.5);
    incTwo = 0.000019635 * pow(92,(36 - awgNumber) / 19.5);
    kcm = mmTwo / 0.5067;
    cuR = 17.24 / mmTwo;
    alR = 28.2 / mmTwo;
    cuRFT = 0.30479 * cuR;
    alRFT = 0.30479 * alR;
    
    //Decimal Formatting
    NSNumberFormatter *df3 = [[NSNumberFormatter alloc] init];
    df3.decimalSeparator = @".";
    df3.minimumFractionDigits = 1;
    df3.maximumFractionDigits = 3;
    NSNumberFormatter *df4 = [[NSNumberFormatter alloc] init];
    df4.decimalSeparator = @".";
    df4.minimumFractionDigits = 1;
    df4.maximumFractionDigits = 4;
    NSNumberFormatter *df5 = [[NSNumberFormatter alloc] init];
    df5.decimalSeparator = @".";
    df5.minimumFractionDigits = 1;
    df5.maximumFractionDigits = 5;
    NSNumberFormatter *df6 = [[NSNumberFormatter alloc] init];
    df6.decimalSeparator = @".";
    df6.minimumFractionDigits = 1;
    df6.maximumFractionDigits = 6;
    NSNumberFormatter *df7 = [[NSNumberFormatter alloc] init];
    df7.decimalSeparator = @".";
    df7.minimumFractionDigits = 1;
    df7.maximumFractionDigits = 7;
    
    
      [self.view endEditing:NO];
    
    //user different formatting depending on awg gauge and displaying the calculated number into respective fields
    if(awgNumber <= 30){
        NSString *formattedInches = [df4 stringFromNumber:[NSNumber numberWithDouble:inc]];
        inchesTF.text = formattedInches;
        NSString *formattedMillim = [df3 stringFromNumber:[NSNumber numberWithDouble:mm]];
        mmTF.text = formattedMillim;
        NSString *formattedMillim2 = [df3 stringFromNumber:[NSNumber numberWithDouble:mmTwo]];
        mm2TF.text = formattedMillim2;
        NSString *formattedInches2 = [df5 stringFromNumber:[NSNumber numberWithDouble:incTwo]];
        inches2TF.text = formattedInches2;
        NSString *formattedKCM = [df3 stringFromNumber:[NSNumber numberWithDouble:kcm]];
        kcmTF.text = formattedKCM;
        
    }else if(awgNumber > 30 && awgNumber < 40){
        
        NSString *formattedInches = [df5 stringFromNumber:[NSNumber numberWithDouble:inc]];
        inchesTF.text = formattedInches;
        NSString *formattedMillim = [df3 stringFromNumber:[NSNumber numberWithDouble:mm]];
        mmTF.text = formattedMillim;
        NSString *formattedMillim2 = [df4 stringFromNumber:[NSNumber numberWithDouble:mmTwo]];
        mm2TF.text = formattedMillim2;
        NSString *formattedInches2 = [df7 stringFromNumber:[NSNumber numberWithDouble:incTwo]];
        inches2TF.text = formattedInches2;
        NSString *formattedKCM = [df4 stringFromNumber:[NSNumber numberWithDouble:kcm]];
        kcmTF.text = formattedKCM;
       
    }else{
        NSString *formattedInches = [df5 stringFromNumber:[NSNumber numberWithDouble:inc]];
        inchesTF.text = formattedInches;
        NSString *formattedMillim = [df4 stringFromNumber:[NSNumber numberWithDouble:mm]];
        mmTF.text = formattedMillim;
        NSString *formattedMillim2 = [df6 stringFromNumber:[NSNumber numberWithDouble:mmTwo]];
        mm2TF.text = formattedMillim2;
        NSString *formattedInches2 = [df7 stringFromNumber:[NSNumber numberWithDouble:incTwo]];
        inches2TF.text = formattedInches2;
        NSString *formattedKCM = [df5 stringFromNumber:[NSNumber numberWithDouble:kcm]];
        kcmTF.text = formattedKCM;
        
    }
    
    
    NSString *formattedCopperR = [df4 stringFromNumber:[NSNumber numberWithDouble:cuR]];
    cuRTF.text = formattedCopperR;
    NSString *formattedaluminumR = [df4 stringFromNumber:[NSNumber numberWithDouble:alR]];
    alRTF.text = formattedaluminumR;
    NSString *formattedCopperRFT = [df4 stringFromNumber:[NSNumber numberWithDouble:cuRFT]];
    curftTF.text = formattedCopperRFT;
    NSString *formattedaluminumRFT = [df4 stringFromNumber:[NSNumber numberWithDouble:alRFT]];
    alrftTF.text = formattedaluminumRFT;
    
    
    

    
    
    
}



//No editing the fields!
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    

    awgPK.delegate = self;
    awgPK.dataSource = self;
    awgPK.showsSelectionIndicator = NO;
    [self.view addSubview:awgPK];
    

    
    inchesTF.userInteractionEnabled = NO;
    mmTF.userInteractionEnabled = NO;
    inches2TF.userInteractionEnabled = NO;
    mm2TF.userInteractionEnabled = NO;
    kcmTF.userInteractionEnabled = NO;
    alrftTF.userInteractionEnabled = NO;
    alRTF.userInteractionEnabled = NO;
    cuRTF.userInteractionEnabled = NO;
    curftTF.userInteractionEnabled = NO;
    
    /*
    CGAffineTransform t0 = CGAffineTransformMakeTranslation (2, awgPK.bounds.size.height/3);
    CGAffineTransform s0 = CGAffineTransformMakeScale       (.50, 0.35);
    CGAffineTransform t1 = CGAffineTransformMakeTranslation (2, -awgPK.bounds.size.height/3);
    awgPK.transform = CGAffineTransformConcat          (t0, CGAffineTransformConcat(s0, t1));
    */
    
    self.title = @"AWG Size Calculator";
  
    CGRect frame = awgPK.frame;
    frame.size.width = 145; // width to be displayed on popover controller
    awgPK.frame = frame;

   
        // Do any additional setup after loading the view, typically from a nib.
    [self configureView];
    
    
   

}







- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
