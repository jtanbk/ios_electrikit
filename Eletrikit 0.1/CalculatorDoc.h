//
//  CalculatorDoc.h
//  Electrikit 0.1
//
//  Created by Jeremiah on 6/26/18.
//  Copyright (c) 2018 Jeremiah. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class CalculatorInfo;

@interface CalculatorDoc : NSObject

@property (strong) CalculatorInfo *data;
@property (strong) UIImage *thumbImage;



- (id)initWithTitle:(NSString*)title rvalue:(float)rvalue thumbImage:(UIImage *)thumbImage;

@end