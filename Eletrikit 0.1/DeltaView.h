//
//  DeltaView.h
//  Electrikit 0.1
//
//  Created by Jeremiah on 6/27/18.
//  Copyright (c) 2018 Jeremiah. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DeltaView : UIViewController <UIApplicationDelegate, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *raTF;
@property (weak, nonatomic) IBOutlet UITextField *rbTF;
@property (weak, nonatomic) IBOutlet UITextField *rcTF;
@property (weak, nonatomic) IBOutlet UITextField *r1TF;
@property (weak, nonatomic) IBOutlet UITextField *r2TF;
@property (weak, nonatomic) IBOutlet UITextField *r3TF;





@end
