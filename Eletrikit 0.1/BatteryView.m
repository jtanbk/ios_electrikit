//
//  BatteryView.m
//  Electrikit 0.1
//
//  Created by Jeremiah on 6/27/18.
//  Copyright (c) 2018 Jeremiah. All rights reserved.
//

#import "BatteryView.h"

@interface BatteryView ()

@end

@implementation BatteryView
@synthesize batterycapacityTF, charge_efficiencyTF, currentconsumptionTF, deviceconsumptionTF, numofwakeTF, wake_durationTF, daysTF, hoursTF, averagecurrentTF, deviceconsumptionLB, numofwakeLB, wake_durationLB, averagecurrentLB, averagecurrentLB2, batterycapcityPK, currentconsumptionPK, deviceconsumptionPK, wake_durationPK, advancePK;
double c, wakePH, sleepPH, avgCurr, battLifeD, battLifeH;
int msToHr = 3600000, capacityUnit, cUnit, sUnit, wUnit = 1;
int bc_titleSet, cc_titleSet, wd_titleSet, dc_titleSet, av_titleSet;
bool advButtonSet = false;


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
      self.title = @"Battery Life Calculator";
    
   
    
    
    
 

//Setting tags for each picker and assigning delegate to itself
    batterycapcityPK.tag = 0;
    batterycapcityPK.delegate = self;
    batterycapcityPK.dataSource = self;
    batterycapcityPK.showsSelectionIndicator = YES;
    [self.view addSubview:batterycapcityPK];
    
    currentconsumptionPK.tag = 1;
    currentconsumptionPK.delegate = self;
    currentconsumptionPK.dataSource = self;
    currentconsumptionPK.showsSelectionIndicator = YES;
    [self.view addSubview:currentconsumptionPK];
    
    deviceconsumptionPK.tag = 2;
    deviceconsumptionPK.delegate = self;
    deviceconsumptionPK.dataSource = self;
    deviceconsumptionPK.showsSelectionIndicator = YES;
    [self.view addSubview:deviceconsumptionPK];

    wake_durationPK.tag = 3;
    wake_durationPK.delegate = self;
    wake_durationPK.dataSource = self;
    wake_durationPK.showsSelectionIndicator = YES;
    [self.view addSubview:wake_durationPK];
    
    advancePK.tag = 4;
    advancePK.delegate = self;
    advancePK.dataSource = self;
    advancePK.showsSelectionIndicator = YES;
    [self.view addSubview:advancePK];
    
    //Adjust width for picker
    CGRect frame = batterycapcityPK.frame;
    frame.size.width = 75; // width to be displayed on popover controller
    batterycapcityPK.frame = frame;
    frame = currentconsumptionPK.frame;
    frame.size.width = 75; // width to be displayed on popover controller
    currentconsumptionPK.frame = frame;
    frame = deviceconsumptionPK.frame;
    frame.size.width = 75; // width to be displayed on popover controller
    deviceconsumptionPK.frame = frame;
    frame = wake_durationPK.frame;
    frame.size.width = 75; // width to be displayed on popover controller
    wake_durationPK.frame = frame;
   
    
    
    /*
    //Adjust picker size
    CGAffineTransform b0 = CGAffineTransformMakeTranslation (0, batterycapcityPK.bounds.size.height/1.2);
    CGAffineTransform c0 = CGAffineTransformMakeScale       (.7, 0.5);
    CGAffineTransform b1 = CGAffineTransformMakeTranslation (0, -batterycapcityPK.bounds.size.height/1.2);
    batterycapcityPK.transform = CGAffineTransformConcat          (b0, CGAffineTransformConcat(c0, b1));
    CGAffineTransform t0 = CGAffineTransformMakeTranslation (0, currentconsumptionPK.bounds.size.height/1.2);
    CGAffineTransform s0 = CGAffineTransformMakeScale       (.9, 0.5);
    CGAffineTransform t1 = CGAffineTransformMakeTranslation (0, -currentconsumptionPK.bounds.size.height/1.2);
    currentconsumptionPK.transform = CGAffineTransformConcat          (t0, CGAffineTransformConcat(s0, t1));
    CGAffineTransform f0 = CGAffineTransformMakeTranslation (0, deviceconsumptionPK.bounds.size.height/1.2);
    CGAffineTransform e0 = CGAffineTransformMakeScale       (.7, 0.5);
    CGAffineTransform f1 = CGAffineTransformMakeTranslation (0, -deviceconsumptionPK.bounds.size.height/1.2);
    deviceconsumptionPK.transform = CGAffineTransformConcat          (f0, CGAffineTransformConcat(e0, f1));
    CGAffineTransform w0 = CGAffineTransformMakeTranslation (0, wake_durationPK.bounds.size.height/1.2);
    CGAffineTransform d0 = CGAffineTransformMakeScale       (1, 0.5);
    CGAffineTransform w1 = CGAffineTransformMakeTranslation (0, -wake_durationPK.bounds.size.height/1.2);
    wake_durationPK.transform = CGAffineTransformConcat          (w0, CGAffineTransformConcat(d0, w1));
    */
    
    //Textfields not editable
    daysTF.userInteractionEnabled = NO;
    hoursTF.userInteractionEnabled = NO;
    averagecurrentTF.userInteractionEnabled = NO;
    

   
    
    
}
//Set up pickerviews titles using tags

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    if (pickerView.tag == 0){
    NSString * bc_title = nil;
    switch(row) {
        case 0:
            bc_title = @"mAh";
            break;
        case 1:
            bc_title = @"Ah";
            break;
    }
    return bc_title;
    }else if (pickerView.tag == 1){
        NSString * cc_title = nil;
        switch(row) {
            case 0:
                cc_title = @"mA";
                break;
            case 1:
                cc_title = @"A";
                break;
        }
        return cc_title;
    }else if (pickerView.tag == 2){
        NSString * dc_title = nil;
        switch(row) {
            case 0:
                dc_title = @"mA";
                break;
            case 1:
                dc_title = @"A";
                break;
        }
        return dc_title;
    }else if (pickerView.tag == 3){
        NSString * wd_title = nil;
        switch(row) {
            case 0:
                wd_title = @"ms";
                break;
            case 1:
                wd_title = @"s";
            break;
            case 2:
                wd_title = @"mins";
                break;
            case 3:
                wd_title = @"hours";
                break;
        }
        return wd_title;
    }else if (pickerView.tag == 4){
        NSString * av_title = nil;
        switch(row) {
            case 0:
                av_title = @"Advance";
                break;
            case 1:
                av_title = @"Basic";
                break;
        }
        return av_title;
    }
    return 0;
}

//Setting up conditional values for picker
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    if (pickerView.tag == 0){
        switch(row) {
            case 0:
                bc_titleSet = 0;
                break;
            case 1:
                bc_titleSet = 1;
                break;
        }
       
    }else if (pickerView.tag == 1){
        switch(row) {
            case 0:
                cc_titleSet = 0;
                break;
            case 1:
                cc_titleSet = 1;
                break;
        }
    }else if (pickerView.tag == 2){
        switch(row) {
            case 0:
                dc_titleSet = 0;
                break;
            case 1:
                dc_titleSet = 1;
                break;
        }
    
    }else if (pickerView.tag == 3){
        switch(row) {
            case 0:
                wd_titleSet = 0;
                break;
            case 1:
                wd_titleSet = 1;
                break;
            case 2:
                wd_titleSet = 2;
                break;
            case 3:
                wd_titleSet = 3;
                break;
        }
    }else if (pickerView.tag == 4){
            switch(row) {
                case 0:
                    //Hiding certain things depending on the mode
                    deviceconsumptionTF.hidden = NO;
                    deviceconsumptionLB.hidden = NO;
                    numofwakeTF.hidden = NO;
                    numofwakeLB.hidden = NO;
                    wake_durationTF.hidden = NO;
                    wake_durationLB.hidden = NO;
                    averagecurrentTF.hidden = NO;
                    averagecurrentLB.hidden = NO;
                    averagecurrentLB2.hidden = NO;
                    wake_durationPK.hidden = NO;
                    deviceconsumptionPK.hidden = NO;
                    
                    break;
                case 1:
                    //Hiding certain things depending on the mode
                    deviceconsumptionTF.hidden = YES;
                    deviceconsumptionLB.hidden = YES;
                    numofwakeTF.hidden = YES;
                    numofwakeLB.hidden = YES;
                    wake_durationTF.hidden = YES;
                    wake_durationLB.hidden = YES;
                    averagecurrentTF.hidden = YES;
                    averagecurrentLB.hidden = YES;
                    averagecurrentLB2.hidden = YES;
                    wake_durationPK.hidden = YES;
                    deviceconsumptionPK.hidden = YES;
                    break;
            }
        
    
    
}
    }
    


- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
   
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if(pickerView.tag == 3){
       
        return 4;
    }else{
    return 2;
}
}

//Calculation
-(IBAction)pressCalc:(id)sender{
    //store all the values from the textfields
    float batterycapacityNum = [batterycapacityTF.text floatValue];
    float currentconsumptionNum = [currentconsumptionTF.text floatValue];
    float charge_efficiencyNum = [charge_efficiencyTF.text floatValue];
    float deviceconsumptionNum = [deviceconsumptionTF.text floatValue];
    int numofwakeNum = [numofwakeTF.text intValue];
    float wake_durationNum = [wake_durationTF.text floatValue];
    
    /*
    NSLog(@"Int value is %f", batterycapacityNum);
    NSLog(@"Int value is %f", charge_efficiencyNum);
    NSLog(@"Int value is %i", currentconsumptionNum);
    NSLog(@"Int value is %f", numofwakeNum);
    NSLog(@"Int value is %f", wake_durationNum);
    */
    
    //Decimal Formatting
    NSNumberFormatter *df3 = [[NSNumberFormatter alloc] init];
    df3.decimalSeparator = @".";
    df3.minimumFractionDigits = 1;
    df3.maximumFractionDigits = 3;
    NSNumberFormatter *df4 = [[NSNumberFormatter alloc] init];
    df4.decimalSeparator = @".";
    df4.minimumFractionDigits = 1;
    df4.maximumFractionDigits = 4;

    
    //Unit conversions
    if(bc_titleSet == 0){
        capacityUnit = 1;
    }else{
        capacityUnit = 1000;
    }
    if(cc_titleSet == 0){
        cUnit = 1;
    }else{
        cUnit = 1000;
    }if(dc_titleSet == 0){
        sUnit = 1;
    }else{
        sUnit = 1000;
    }if(wd_titleSet == 0){
        wUnit = 1;
    }else if(wd_titleSet == 1){
        wUnit = 1000;
    }else if(wd_titleSet == 2){
        wUnit = 1000 * 60;
    }else  if(wd_titleSet == 3){
        wUnit = 1000 * 60 * 60;
    }else{
    }
    
    
    //calculations of battery life
    c = batterycapacityNum * capacityUnit * charge_efficiencyNum * 0.01;
    // NSLog(@"value for c: %f \n", c);
    wakePH = numofwakeNum * wake_durationNum * wUnit;
    //NSLog(@"value for wakePH: %f \n", wakePH);
    sleepPH = msToHr - wakePH;
    // NSLog(@"value for sleepPH: %f \n", sleepPH);
    avgCurr = ((currentconsumptionNum * cUnit *wakePH) + (deviceconsumptionNum *sUnit * (sleepPH) )) / msToHr;
    // NSLog(@"value for avgCurr: %f \n", avgCurr);
    
    
  
    //If basic
    if (numofwakeTF.hidden == TRUE){
         NSLog(@"value for c: %f \n", c);
         NSLog(@"value for ccNum: %f \n", currentconsumptionNum);
         NSLog(@"value for cUnit: %i \n", cUnit);
        
         battLifeH = c / (currentconsumptionNum * cUnit);
    }else{
        battLifeH = c /avgCurr;
       
    }
    
    
    
    battLifeD = battLifeH / 24;
    
    NSString *batteryLifeDay = [df3 stringFromNumber:[NSNumber numberWithDouble:battLifeD]];
    daysTF.text = batteryLifeDay;
    NSString *batteryLifeHour = [df3 stringFromNumber:[NSNumber numberWithDouble:battLifeH]];
    hoursTF.text = batteryLifeHour;
    NSString *avgCurrConsumption = [df4 stringFromNumber:[NSNumber numberWithDouble:avgCurr]];
    averagecurrentTF.text = avgCurrConsumption;
    
    
    
}



-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    return YES;
}


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    
    [self.view endEditing:YES];
    return YES;
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

//Moving the keyboard if covering textfields
- (void)keyboardDidShow:(NSNotification *)notification
{
    // Assign new frame to your view
    [self.view setFrame:CGRectMake(0,-110,320,460)]; //here taken -110 for example i.e. your view will be scrolled to -110. change its value according to your requirement.
    
}

-(void)keyboardDidHide:(NSNotification *)notification
{
    [self.view setFrame:CGRectMake(0,0,320,460)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
