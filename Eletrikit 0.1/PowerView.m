//
//  PowerView.m
//  Electrikit 0.1
//
//  Created by Jeremiah on 7/11/18.
//  Copyright (c) 2018 Jeremiah. All rights reserved.
//

#import "PowerView.h"
#include "math.h"
@interface PowerView ()

@end

@implementation PowerView

@synthesize voltagellTF, voltagelnTF, phaseTF, currentTF, powerfactTF, truepowerTF1, apparentpowerTF1, reactivepowerTF1, truepowerTF2, apparentpowerTF2, reactivepowerTF2, totaltitleLB, truepowerLB1, apparentpowerLB1, reactivepowerLB1, powertitleLB, truepowerLB2, apparentpowerLB2, reactivepowerLB2, truepowerwLB1, truepowerwLB2, apparentpowervaLB1, apparentpowervaLB2, reactivepowervarLB1, reactivepowervarLB2, powertrianglePK, llLB, voltagelnLB, voltagelnvLB, phasedegreeLB, phaseLB, powerfactLB1, powerfactLB2;

int powertriangleSet = 0;
int resetNum = 0;




- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"Power Calculator";
    
    powertrianglePK.delegate = self;
    powertrianglePK.dataSource = self;
    powertrianglePK.showsSelectionIndicator = YES;
    [self.view addSubview:powertrianglePK];
    /*
    //Picker size adjustment
    CGAffineTransform b0 = CGAffineTransformMakeTranslation (0, powertrianglePK.bounds.size.height/1.3);
    CGAffineTransform c0 = CGAffineTransformMakeScale       (.8, 0.6);
    CGAffineTransform b1 = CGAffineTransformMakeTranslation (0, -powertrianglePK.bounds.size.height/1.3);
    powertrianglePK.transform = CGAffineTransformConcat          (b0, CGAffineTransformConcat(c0, b1));
    */
    //Setting tag
    powerfactTF.tag = 0;
    phaseTF.tag = 1;
    voltagellTF.tag = 2;
    voltagelnTF.tag = 3;
    
    //Able to call this function in these textboxes
    [self.phaseTF addTarget:self action:@selector(textFieldDidEndEditing:) forControlEvents:UIControlEventEditingDidEnd];
    [self.powerfactTF addTarget:self action:@selector(textFieldDidEndEditing:) forControlEvents:UIControlEventEditingDidEnd];
    [self.voltagellTF addTarget:self action:@selector(textFieldDidEndEditing:) forControlEvents:UIControlEventEditingDidEnd];
    [self.voltagelnTF addTarget:self action:@selector(textFieldDidEndEditing:) forControlEvents:UIControlEventEditingDidEnd];
    
    
    //Textfields not editable
    truepowerTF1.userInteractionEnabled = NO;
    truepowerTF2.userInteractionEnabled = NO;
    apparentpowerTF1.userInteractionEnabled = NO;
    apparentpowerTF2.userInteractionEnabled = NO;
    reactivepowerTF1.userInteractionEnabled = NO;
    reactivepowerTF2.userInteractionEnabled = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//Power Triangle picker list
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    NSString * title = nil;
    switch(row) {
        case 0:
            title = @"AC-Three Phase";
            break;
        case 1:
             title = @"AC-Single Phase";
            break;
        case 2:
            title = @"DC";
            break;
    }
    return title;
}


//Different views selected by the picker
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
        switch(row) {
            case 0:{
                voltagellTF.hidden = NO;
                voltagelnTF.hidden = NO;
                phaseTF.hidden = NO;
                currentTF.hidden = NO;
                powerfactTF.hidden = NO;
                truepowerTF1.hidden = NO;
                apparentpowerTF1.hidden = NO;
                reactivepowerTF1.hidden = NO;
                truepowerTF2.hidden = NO;
                apparentpowerTF2.hidden = NO;
                reactivepowerTF2.hidden = NO;
                totaltitleLB.hidden = NO;
                truepowerLB1.hidden = NO;
                apparentpowerLB1.hidden = NO;
                reactivepowerLB1.hidden = NO;
                powertitleLB.hidden = NO;
                truepowerLB2.hidden = NO;
                apparentpowerLB2.hidden = NO;
                reactivepowerLB2.hidden = NO;
                truepowerwLB1.hidden = NO;
                truepowerwLB2.hidden = NO;
                apparentpowervaLB1.hidden = NO;
                apparentpowervaLB2.hidden = NO;
                reactivepowervarLB1.hidden = NO;
                reactivepowervarLB2.hidden = NO;
                voltagelnLB.hidden = NO;
                voltagelnvLB.hidden = NO;
                phaseLB.hidden = NO;
                phasedegreeLB.hidden = NO;
                powerfactLB1.hidden = NO;
                powerfactLB2.hidden = NO;
                powertriangleSet = 0;
                //Setting text according picker type
                NSString *voltagellString = [NSString stringWithFormat:@"Voltage (L-L)"];
                llLB.text = voltagellString;
                break;
            }
            case 1:{
                voltagellTF.hidden = NO;
                voltagelnTF.hidden = YES;
                phaseTF.hidden = NO;
                currentTF.hidden = NO;
                powerfactTF.hidden = NO;
                truepowerTF1.hidden = NO;
                apparentpowerTF1.hidden = NO;
                reactivepowerTF1.hidden = NO;
                truepowerTF2.hidden = YES;
                apparentpowerTF2.hidden = YES;
                reactivepowerTF2.hidden = YES;
                totaltitleLB.hidden = NO;
                truepowerLB1.hidden = NO;
                apparentpowerLB1.hidden = NO;
                reactivepowerLB1.hidden = NO;
                powertitleLB.hidden = YES;
                truepowerLB2.hidden = YES;
                apparentpowerLB2.hidden = YES;
                reactivepowerLB2.hidden = YES;
                truepowerwLB1.hidden = NO;
                truepowerwLB2.hidden = YES;
                apparentpowervaLB1.hidden = NO;
                apparentpowervaLB2.hidden = YES;
                reactivepowervarLB1.hidden = NO;
                reactivepowervarLB2.hidden = YES;
                voltagelnLB.hidden = YES;
                voltagelnvLB.hidden = YES;
                phaseLB.hidden = NO;
                phasedegreeLB.hidden = NO;
                powerfactLB2.hidden = NO;
                powerfactLB1.hidden = NO;
                powertriangleSet = 1;
                //Setting text according picker type
                NSString *voltagellString = [NSString stringWithFormat:@"Voltage"];
                llLB.text = voltagellString;
                break;
            }
            case 2:{
                voltagellTF.hidden = NO;
                voltagelnTF.hidden = YES;
                phaseTF.hidden = YES;
                currentTF.hidden = NO;
                powerfactTF.hidden = YES;
                truepowerTF1.hidden = NO;
                apparentpowerTF1.hidden = YES;
                reactivepowerTF1.hidden = YES;
                truepowerTF2.hidden = YES;
                apparentpowerTF2.hidden = YES;
                reactivepowerTF2.hidden = YES;
                totaltitleLB.hidden = YES;
                truepowerLB1.hidden = NO;
                apparentpowerLB1.hidden = YES;
                reactivepowerLB1.hidden = YES;
                powertitleLB.hidden = YES;
                truepowerLB2.hidden = YES;
                apparentpowerLB2.hidden = YES;
                reactivepowerLB2.hidden = YES;
                truepowerwLB1.hidden = NO;
                truepowerwLB2.hidden = YES;
                apparentpowervaLB1.hidden = YES;
                apparentpowervaLB2.hidden = YES;
                reactivepowervarLB1.hidden = YES;
                reactivepowervarLB2.hidden = YES;
                voltagelnLB.hidden = YES;
                voltagelnvLB.hidden = YES;
                phaseLB.hidden = YES;
                phasedegreeLB.hidden = YES;
                powerfactLB2.hidden = YES;
                powerfactLB1.hidden = YES;
                powertriangleSet = 2;
                //Setting text according picker type
                NSString *voltagellString = [NSString stringWithFormat:@"Voltage"];
                llLB.text = voltagellString;
                break;
            }
        }

}


- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return 3;
}



- (void)textFieldDidEndEditing:(UITextField *)textfield {
    //Decimal Formatting
    NSNumberFormatter *df3 = [[NSNumberFormatter alloc] init];
    df3.decimalSeparator = @".";
    df3.minimumFractionDigits = 1;
    df3.maximumFractionDigits = 3;
    
    NSNumberFormatter *dfd3 = [[NSNumberFormatter alloc] init];
    dfd3.decimalSeparator = @".";
    dfd3.minimumFractionDigits = 1;
    dfd3.maximumFractionDigits = 3;
    [dfd3 setRoundingMode: NSNumberFormatterRoundDown];

    
    NSNumberFormatter *df1 = [[NSNumberFormatter alloc] init];
    df1.decimalSeparator = @".";
    df1.minimumFractionDigits = 1;
    df1.maximumFractionDigits = 1;
    df1.maximumIntegerDigits = 2;
   
    
    //Storing values from textfield
    float phaseNum = [phaseTF.text floatValue];
    float voltagelnNum = [voltagelnTF.text floatValue];
    float voltagellNum = [voltagellTF.text floatValue];
    float powerfactNum = [powerfactTF.text floatValue];
    //Checking if values are in range then displaying an alert if not
    if(phaseNum > 90.0 || phaseNum < 0.0){
        NSString *title1 = @"Check Submitted Values";
        NSString *message1 = @"The range in Phase can only be from 0 to 90";
        NSString *okText1 = @"OK";
        UIAlertController * alert = [UIAlertController alertControllerWithTitle:title1 message:message1 preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okButton  = [UIAlertAction actionWithTitle:okText1 style:UIAlertActionStyleCancel handler:nil];
        [alert addAction:okButton];
        [self presentViewController:alert animated:YES completion:nil];
        NSString *resetnumString = [df3 stringFromNumber:[NSNumber numberWithDouble:resetNum]];
        phaseTF.text = resetnumString;
        NSString *resetnumString2 = [df3 stringFromNumber:[NSNumber numberWithDouble:resetNum]];
        powerfactTF.text = resetnumString2;
        
    }else if(powerfactNum > 1.0 || powerfactNum < 0.0 ){
        NSString *title2 = @"Check Submitted Values";
        NSString *message2 = @"The range in Power Factor can only be from 0 to 1";
        NSString *okText2 = @"OK";
        UIAlertController * alert = [UIAlertController alertControllerWithTitle:title2 message:message2 preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okButton  = [UIAlertAction actionWithTitle:okText2 style:UIAlertActionStyleCancel handler:nil];
        [alert addAction:okButton];
        [self presentViewController:alert animated:YES completion:nil];
        NSString *resetnumString = [df3 stringFromNumber:[NSNumber numberWithDouble:resetNum]];
        powerfactTF.text = resetnumString;
        NSString *resetnumString2 = [df3 stringFromNumber:[NSNumber numberWithDouble:resetNum]];
        phaseTF.text = resetnumString2;
    }else{
    
    //Phase and powerfactor conversions
    if(textfield.tag == 1){
        //Phase to Power
    powerfactNum = phaseNum * (M_PI / 180.0f);
    powerfactNum = cosf(powerfactNum);
    NSString *powerfactnumString = [df3 stringFromNumber:[NSNumber numberWithDouble:powerfactNum]];
    powerfactTF.text = powerfactnumString;
        }else if(textfield.tag == 0){
        //Power to Phase
            phaseNum = acosf(powerfactNum);
            phaseNum = phaseNum * (180.0f/M_PI);
            NSString *phasenumString = [df1 stringFromNumber:[NSNumber numberWithDouble:phaseNum]];
            phaseTF.text = phasenumString;
        }else if(textfield.tag ==2){
            //voltageLL to Voltage LN
            voltagelnNum = (voltagellNum / sqrt(3.0));
            NSString *voltagelnString = [df3 stringFromNumber:[NSNumber numberWithDouble:voltagelnNum]];
            voltagelnTF.text = voltagelnString;
            
        }else if(textfield.tag == 3){
            //VoltageLN to Voltage LL
            voltagellNum = (sqrt(3.0) * voltagelnNum);
            NSString *voltagellString = [dfd3 stringFromNumber:[NSNumber numberWithDouble:voltagellNum]];
            voltagellTF.text = voltagellString;
        }
    }
    
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

-(IBAction)pressCalc:(id)sender{
    //Decimal Formatting
    NSNumberFormatter *df3 = [[NSNumberFormatter alloc] init];
    df3.decimalSeparator = @".";
    df3.minimumFractionDigits = 1;
    df3.maximumFractionDigits = 3;;
    NSNumberFormatter *df4d = [[NSNumberFormatter alloc] init];
    df4d.decimalSeparator = @".";
    df4d.minimumFractionDigits = 1;
    df4d.maximumFractionDigits = 3;
    [df4d setRoundingMode: NSNumberFormatterRoundDown];
    
    NSNumberFormatter *df3up = [[NSNumberFormatter alloc] init];
    df3up.decimalSeparator = @".";
    df3up.minimumFractionDigits = 1;
    df3up.maximumFractionDigits = 3;
    [df3up setRoundingMode: NSNumberFormatterRoundUp];
    
    //store all the values from the textfields
    float voltagellNum = [voltagellTF.text floatValue];
    float currentNum = [currentTF.text floatValue];
    float powerfactNum = [powerfactTF.text floatValue];
    float truepowerNum1 = 0;
    float apparentpowerNum1 = 0;
    float reactivepowerNum1 = 0;
    float truepowerNum2 = 0;
    float apparentpowerNum2 = 0;
    float reactivepowerNum2 = 0;
    
    
    if(powertriangleSet == 0){
        //AC Three
        truepowerNum1 = (sqrt(3.0) * voltagellNum * currentNum * powerfactNum);
        apparentpowerNum1 = (truepowerNum1 / powerfactNum);
        reactivepowerNum1 = (sqrt(pow(apparentpowerNum1, 2) - pow(truepowerNum1,2)));
        truepowerNum2 = (truepowerNum1 / 3);
        apparentpowerNum2 = (apparentpowerNum1 / 3);
        reactivepowerNum2 = (reactivepowerNum1 / 3);
        
        //Set Text
        NSString *truepowerString1 = [df4d stringFromNumber:[NSNumber numberWithDouble:truepowerNum1]];
        truepowerTF1.text = truepowerString1;
        NSString *apparentpowerString1 = [df3up stringFromNumber:[NSNumber numberWithDouble:apparentpowerNum1]];
        apparentpowerTF1.text = apparentpowerString1;
        NSString *reactivepowerString1 = [df3up stringFromNumber:[NSNumber numberWithDouble:reactivepowerNum1]];
        reactivepowerTF1.text = reactivepowerString1;
        NSString *truepowerString2 = [df4d stringFromNumber:[NSNumber numberWithDouble:truepowerNum2]];
        truepowerTF2.text = truepowerString2;
        NSString *apparentpowerString2 = [df3up stringFromNumber:[NSNumber numberWithDouble:apparentpowerNum2]];
        apparentpowerTF2.text = apparentpowerString2;
        NSString *reactivepowerString2 = [df3up stringFromNumber:[NSNumber numberWithDouble:reactivepowerNum2]];
        reactivepowerTF2.text = reactivepowerString2;
    }else if(powertriangleSet == 1){
        //AC One
         truepowerNum1 = (voltagellNum * currentNum * powerfactNum);
         apparentpowerNum1 = (truepowerNum1 / powerfactNum);
         reactivepowerNum1 = (sqrt(pow(apparentpowerNum1, 2) - pow(truepowerNum1,2)));
         NSString *truepowerString1 = [df4d stringFromNumber:[NSNumber numberWithDouble:truepowerNum1]];
         truepowerTF1.text = truepowerString1;
        NSString *apparentpowerString1 = [df3up stringFromNumber:[NSNumber numberWithDouble:apparentpowerNum1]];
         apparentpowerTF1.text = apparentpowerString1;
        NSString *reactivepowerString1 = [df3up stringFromNumber:[NSNumber numberWithDouble:reactivepowerNum1]];
        reactivepowerTF1.text = reactivepowerString1;

    }else if (powertriangleSet == 2){
        //DC
         truepowerNum1 = (voltagellNum * currentNum);
        NSString *truepowerString1 = [df4d stringFromNumber:[NSNumber numberWithDouble:truepowerNum1]];
        truepowerTF1.text = truepowerString1;
    }

    
        
        
        
    
    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
