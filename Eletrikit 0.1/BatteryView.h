//
//  BatteryView.h
//  Electrikit 0.1
//
//  Created by Jeremiah on 6/27/18.
//  Copyright (c) 2018 Jeremiah. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BatteryView : UIViewController<UIPickerViewDelegate, UIPickerViewDataSource>


-(IBAction)pressCalc:(id)sender;



@property (weak, nonatomic) IBOutlet UITextField *batterycapacityTF;
@property (weak, nonatomic) IBOutlet UITextField *charge_efficiencyTF;
@property (weak, nonatomic) IBOutlet UITextField *currentconsumptionTF;
@property (weak, nonatomic) IBOutlet UITextField *deviceconsumptionTF;
@property (weak, nonatomic) IBOutlet UITextField *numofwakeTF;
@property (weak, nonatomic) IBOutlet UITextField *wake_durationTF;
@property (weak, nonatomic) IBOutlet UITextField *daysTF;
@property (weak, nonatomic) IBOutlet UITextField *hoursTF;
@property (weak, nonatomic) IBOutlet UITextField *averagecurrentTF;

@property(weak, nonatomic) IBOutlet UILabel *deviceconsumptionLB;
@property(weak, nonatomic) IBOutlet UILabel *numofwakeLB;
@property(weak, nonatomic) IBOutlet UILabel *wake_durationLB;
@property(weak, nonatomic) IBOutlet UILabel *averagecurrentLB;
@property(weak, nonatomic) IBOutlet UILabel *averagecurrentLB2;


@property (strong, nonatomic) IBOutlet UIPickerView *batterycapcityPK;
@property (strong, nonatomic) IBOutlet UIPickerView *currentconsumptionPK;
@property (strong, nonatomic) IBOutlet UIPickerView *deviceconsumptionPK;
@property (strong, nonatomic) IBOutlet UIPickerView *wake_durationPK;
@property (strong, nonatomic) IBOutlet UIPickerView *advancePK;




@end
