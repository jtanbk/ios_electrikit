//
//  HorsepowerView.h
//  Electrikit 0.1
//
//  Created by Jeremiah on 7/10/18.
//  Copyright (c) 2018 Jeremiah. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HorsepowerView : UIViewController<UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *voltageTF;
@property (weak, nonatomic) IBOutlet UITextField *currentTF;
@property (weak, nonatomic) IBOutlet UITextField *kilowattsTF;
@property (weak, nonatomic) IBOutlet UITextField *kilovoltTF;
@property (weak, nonatomic) IBOutlet UITextField *powerfactorTF;
@property (weak, nonatomic) IBOutlet UITextField *efficiencyTF;
@property (weak, nonatomic) IBOutlet UITextField *horsepowerTF;
@property (strong, nonatomic) IBOutlet UIPickerView *horsepowerPK;
@property (strong, nonatomic) IBOutlet UIActivityViewController *horsepowerAV;
@property (weak, nonatomic) IBOutlet UILabel *kilovoltLB;
@property (weak, nonatomic) IBOutlet UILabel *kilovoltLB2;
@property (weak, nonatomic) IBOutlet UIButton *kilovoltBT;
@property (weak, nonatomic) IBOutlet UILabel *powerfactorLB;



@end
