//
//  MasterViewController.m
//  Electrikit 0.1
//
//  Created by Jeremiah on 6/26/18.
//  Copyright (c) 2018 Jeremiah. All rights reserved.
//

#import "MasterViewController.h"
#import "DetailViewController.h"
#import "CalculatorDoc.h"
#import  "CalculatorInfo.h"
#import <UIKit/UIKit.h>

@interface MasterViewController () {
    NSMutableArray *_objects;
}
@end

@implementation MasterViewController

@synthesize Calculators =_Calculators;
@synthesize masterTV;

- (void)awakeFromNib {
    [super awakeFromNib];
    }





- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
  //  self.tableView.contentInset = UIEdgeInsetsMake(-1.0f, 0.0f, 0.0f, 0.0);
    
   // self.navigationItem.leftBarButtonItem = self.editButtonItem;

    
    /*UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(insertNewObject:)];
    self.navigationItem.rightBarButtonItem = addButton;
    */
    
    masterTV.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    self.title = @"Electrikit";
    
  
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




#pragma mark - Segues



#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _Calculators.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView
                             dequeueReusableCellWithIdentifier:@"MyBasicCell"];
    CalculatorDoc *calc = [self.Calculators objectAtIndex:indexPath.row];
    cell.textLabel.text = calc.data.title; //
    cell.imageView.image = calc.thumbImage;
    return cell;
}

- (void) tableView: (UITableView*) tableView didSelectRowAtIndexPath: (NSIndexPath*) indexPath{
    NSString* identifier = nil;
    switch (indexPath.section){
        case 0:
            switch (indexPath.row) {
                case 0: identifier = @"Identifier0"; break;
                case 1: identifier = @"Identifier1"; break;
                case 2: identifier = @"Identifier2"; break;
                case 3: identifier = @"Identifier3"; break;
                case 4: identifier = @"Identifier4"; break;
                case 5: identifier = @"Identifier5"; break;
                case 6: identifier = @"Identifier6"; break;
                case 7: identifier = @"Identifier7"; break;
                default: break;
            }
            break;
        default:break;
    }
    if (identifier != nil){
        //Segue
        [self.navigationController pushViewController: [[self storyboard] instantiateViewControllerWithIdentifier: identifier] animated: YES];
    }
    else{
        NSAssert(NO, @"No Storyboard Identifer for that one you selected!");
            
            }
    }
    


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return NO;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self.Calculators removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //calculating the height according to the device height.
    return self.view.frame.size.height * (80.0 / 568.0);
}

@end
