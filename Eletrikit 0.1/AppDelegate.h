//
//  AppDelegate.h
//  Electrikit 0.1
//
//  Created by Jeremiah on 6/26/18.
//  Copyright (c) 2018 Jeremiah. All rights reserved.
//
#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
