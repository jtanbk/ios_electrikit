//
//  VoltageDropView.m
//  Electrikit 0.1
//
//  Created by Jeremiah on 7/16/18.
//  Copyright (c) 2018 Jeremiah. All rights reserved.
//

#import "VoltageDropView.h"
#define PICKER_MIN 1
#define Picker_MAX 106

@interface VoltageDropView ()

@end


@implementation VoltageDropView
@synthesize voltageTF, currentTF, wirelengthTF, voltagedropTF, percentdropTF, voltageloadTF, materialPK, wirelengthPK, voltagetypePK, wiresizePK, wiresizeareaLB, wiresizediameterLB;
int materialSet, voltagetypeSet, wirelengthSet;
int phase = 1;

double awgNum = -5;
double inc, mm, mmTwo, inTwo, kcm, cuR, alR, cuRFT, alRFT, vDrop, resistance = 0;



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"Voltage Drop Calculator";
    
    //SET UP picker sizes and set tags
    materialPK.tag = 0;
    materialPK.delegate = self;
    materialPK.dataSource = self;
    materialPK.showsSelectionIndicator = YES;
    [self.view addSubview:materialPK];
    wiresizePK.tag = 1;
    wiresizePK.delegate = self;
    wiresizePK.dataSource = self;
    wiresizePK.showsSelectionIndicator = YES;
    [self.view addSubview:wiresizePK];
    voltagetypePK.tag = 2;
    voltagetypePK.delegate = self;
    voltagetypePK.dataSource = self;
    voltagetypePK.showsSelectionIndicator = YES;
    [self.view addSubview:voltagetypePK];
    wirelengthPK.tag = 3;
    wirelengthPK.delegate = self;
    wirelengthPK.dataSource = self;
    wirelengthPK.showsSelectionIndicator = YES;
    [self.view addSubview:wirelengthPK];
    /*
    //Adjust picker size
    CGAffineTransform a0 = CGAffineTransformMakeTranslation (0, materialPK.bounds.size.height/1.5);
    CGAffineTransform b0 = CGAffineTransformMakeScale       (1, 0.4);
    CGAffineTransform a1 = CGAffineTransformMakeTranslation (0, -materialPK.bounds.size.height/1.5);
    materialPK.transform = CGAffineTransformConcat          (a0, CGAffineTransformConcat(b0, a1));
    
    
    CGAffineTransform c0 = CGAffineTransformMakeTranslation (0, wiresizePK.bounds.size.height/1.5);
    CGAffineTransform d0 = CGAffineTransformMakeScale       (1, 0.4);
    CGAffineTransform c1 = CGAffineTransformMakeTranslation (0, -wiresizePK.bounds.size.height/1.5);
    wiresizePK.transform = CGAffineTransformConcat          (c0, CGAffineTransformConcat(d0, c1));
    
    
    CGAffineTransform e0 = CGAffineTransformMakeTranslation (0, voltagetypePK.bounds.size.height/1.5);
    CGAffineTransform f0 = CGAffineTransformMakeScale       (1, 0.4);
    CGAffineTransform e1 = CGAffineTransformMakeTranslation (0, -voltagetypePK.bounds.size.height/1.5);
    voltagetypePK.transform = CGAffineTransformConcat          (e0, CGAffineTransformConcat(f0, e1));
    
    
    CGAffineTransform g0 = CGAffineTransformMakeTranslation (0, wirelengthPK.bounds.size.height/1.5);
    CGAffineTransform h0 = CGAffineTransformMakeScale       (1, 0.4);
    CGAffineTransform g1 = CGAffineTransformMakeTranslation (0, -wirelengthPK.bounds.size.height/1.5);
    wirelengthPK.transform = CGAffineTransformConcat          (g0, CGAffineTransformConcat(h0, g1));
    */
    
    //Adjust width for picker
    CGRect frame = materialPK.frame;
    frame.size.width = 115; // width to be displayed on popover controller
    materialPK.frame = frame;
    frame = voltagetypePK.frame;
    frame.size.width = 200; // width to be displayed on popover controller
    voltagetypePK.frame = frame;
    frame = wirelengthPK.frame;
    frame.size.width = 75; // width to be displayed on popover controller
    wirelengthPK.frame = frame;
    
    
    //Textfields not editable
    voltagedropTF.userInteractionEnabled = NO;
    percentdropTF.userInteractionEnabled = NO;
    voltageloadTF.userInteractionEnabled = NO;
    
}
//Setting up different picker titles
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    if (pickerView.tag == 0){
        NSString * m_title = nil;
        switch(row) {
            case 0:
                m_title = @"Copper";
                break;
            case 1:
                m_title = @"Aluminum";
               
                break;
        }
        return m_title;
    }
    
    if (pickerView.tag == 1){
        NSString * w_title = nil;
        switch(row) {
            case 0:
                w_title = @"000000";
                break;
            case 1:
                w_title = @"00000";
                break;
            case 2:
                w_title = @"0000";
                break;
            case 3:
                w_title = @"000";
                break;
            case 4:
                w_title = @"00";
                break;
            case 5:
                w_title = @"0";
            default:
                w_title =  [NSString stringWithFormat:@"%ld", (row+PICKER_MIN - 6)];
                
        }
        return w_title;
    }
    
    if (pickerView.tag == 2){
        NSString * v_title = nil;
        switch(row) {
            case 0:
                v_title = @"AC-Single Phase";
                break;
            case 1:
                v_title = @"AC-Three Phase";
                break;
            case 2:
                v_title = @"DC";
                break;
        }
        return v_title;
    }


    if (pickerView.tag == 3){
        NSString * l_title = nil;
        switch(row) {
            case 0:
                l_title = @"meter";
                break;
            case 1:
                l_title = @"feet";
        
        }
        return l_title;
    }

    
    
    
    return  0;
}




- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if (pickerView.tag == 0){
        return 2;
    }else if (pickerView.tag == 1){
        return (Picker_MAX-PICKER_MIN+1);
    }else if (pickerView.tag == 2){
        return 3;
    }else
        return 2;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

//Setting conditions with picker
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    
    //Material
    if (pickerView.tag == 0){
        switch(row) {
            case 0:
                materialSet = 0;
                break;
            case 1:
                materialSet = 1;
                break;
        }
    //Wire Size
    }else if (pickerView.tag == 1){
        
  
        
        switch(row) {
        case 0:
            awgNum = 1 - 6;
            break;
        case 1:
            awgNum = 1 - 5;
            break;
        case 2:
            awgNum = 1 - 4;
            break;
        case 3:
            awgNum = 1 - 3;
            break;
        case 4:
            awgNum = 1 - 2;
            break;
        case 5:
            awgNum = 1 - 1;
            break;
        default:
            awgNum = (row - 5);
                   }
        
        
    }else if (pickerView.tag == 2){
        switch (row) {
            case 0:
                voltagetypeSet = 0;
                break;
            case 1:
                voltagetypeSet = 1;
                break;
            case 2:
                voltagetypeSet = 2;
                break;
        }
    }else if (pickerView.tag == 3){
        switch(row) {
            case 0:
                wirelengthSet = 0;
                break;
            case 1:
                wirelengthSet = 1;
                break;
        }
    }
    
    
    
    inc = 0.005 * pow(92,(36 - (awgNum)) / 39);
    mm = 0.127 * pow(92,(36 - (awgNum)) / 39);
    mmTwo = 0.012668 * pow(92,(36 - (awgNum)) / 19.5);
    inTwo = 0.000019635 * pow(92,(36 - (awgNum)) / 19.5);
    kcm = mmTwo / 0.5067;
    //calculations for resistance of wire for copper and aluminum in ohms/km
    cuR = 17.24 / mmTwo;
    alR = 28.2 / mmTwo;
    //converts resistance to ohms/kft
    cuRFT = 0.30479 * cuR;
    alRFT = 0.30479 * alR;

    //Setting text according to Wire Type
    NSString *wiresizediameterString = [NSString stringWithFormat:@"Diameter: %1.4f in, %1.4f mm", inc, mm];
    wiresizediameterLB.text = wiresizediameterString;
    NSString *wiresizeareaString =[NSString stringWithFormat:@"Area: %1.4f in^2, %1.4f mm^2, %1.4f kcmil", inTwo, mmTwo, kcm];
    wiresizeareaLB.text = wiresizeareaString;
    
    
}


-(IBAction)pressCalc:(id)sender{
    float voltageNum = [voltageTF.text floatValue];
    float currentNum = [currentTF.text floatValue];
    float wirelengthNum = [wirelengthTF.text floatValue];
    float voltageloadNum;
    float percentdropNum;
    
    //Decimal formatting
    NSNumberFormatter *df4UP = [[NSNumberFormatter alloc] init];
    df4UP.decimalSeparator = @".";
    df4UP.minimumFractionDigits = 1;
    df4UP.maximumFractionDigits = 4;
    NSNumberFormatter *df4DOWN = [[NSNumberFormatter alloc] init];
    df4DOWN.decimalSeparator = @".";
    df4DOWN.minimumFractionDigits = 1;
    df4DOWN.maximumFractionDigits = 4;
    [df4DOWN setRoundingMode: NSNumberFormatterRoundDown];
    NSNumberFormatter *df4 = [[NSNumberFormatter alloc] init];
    df4.decimalSeparator = @".";
    df4.minimumFractionDigits = 1;
    df4.maximumFractionDigits = 4;
    [df4 setRoundingMode: NSNumberFormatterRoundUp];
    NSNumberFormatter *df3 = [[NSNumberFormatter alloc] init];
    df3.decimalSeparator = @".";
    df3.minimumFractionDigits = 1;
    df3.maximumFractionDigits = 3;
 
    
    
    
    
   
    //Setting Phase number from picker selection.
    if (materialSet == 0){ // Copper
        if(wirelengthSet == 0){ //Meter
            resistance = cuR;
            if(voltagetypeSet == 2 || voltagetypeSet == 0){
                phase = 1;
            }
            if (voltagetypeSet == 1){
                phase = 3;
            }
        }if (wirelengthSet == 1){
            resistance = cuRFT;
            if(voltagetypeSet == 2 || voltagetypeSet == 0){
                phase = 1;
            }
            if (voltagetypeSet == 1){
                phase = 3;
            }
        }
    }
    if (materialSet == 1){
        if (wirelengthSet == 0){
        resistance = alR;
        if(voltagetypeSet == 2 || voltagetypeSet == 0){
            phase = 1;
        }
        if (voltagetypeSet == 1){
            phase = 3;
        }
        }else if (wirelengthSet == 1){
            resistance = alRFT;
            if (voltagetypeSet == 2 || voltagetypeSet == 0){
                phase = 1;
            }
            if (voltagetypeSet == 1){
                phase = 3;
            }
        }
    }
    //Getting vDrop number according to phase
    if (resistance != 0){
        if (phase == 1){
            vDrop = ((wirelengthNum * resistance) / 1000) * 2 * currentNum;
        }else{
            vDrop = sqrt(3) * currentNum * ((wirelengthNum * resistance) / 1000) * 2 * currentNum;
        }
        
        NSString *voltagedropString = [df4UP stringFromNumber:[NSNumber numberWithDouble:vDrop]];
        voltagedropTF.text = voltagedropString;
        
        //Conditions for voltage and Vdrop
        if ((voltageNum - vDrop) < 0 || ((vDrop / voltageNum) * 100) > 100){
            //When negative set to 0
            voltageloadNum = 0;
            percentdropNum = 100;
            NSString *voltageloadString = [df4UP stringFromNumber:[NSNumber numberWithDouble:voltageloadNum]];
            voltageloadTF.text = voltageloadString;
            NSString *percentdropString = [df3 stringFromNumber:[NSNumber numberWithDouble:percentdropNum]];
            percentdropTF.text = percentdropString;
            
        }else{
            voltageloadNum = voltageNum - vDrop;
            percentdropNum = (vDrop / voltageNum) * 100;
            NSString *voltageloadString = [df4UP stringFromNumber:[NSNumber numberWithDouble:voltageloadNum]];
            voltageloadTF.text = voltageloadString;
            NSString *percentdropString = [df3 stringFromNumber:[NSNumber numberWithDouble:percentdropNum]];
            percentdropTF.text = percentdropString;
            
            
        }
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

