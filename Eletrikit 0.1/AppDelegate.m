//
//  AppDelegate.m
//  Electrikit 0.1
//
//  Created by Jeremiah on 6/26/18.
//  Copyright (c) 2018 Jeremiah. All rights reserved.
//

#import "AppDelegate.h"
#import "DetailViewController.h"
#import "MasterViewController.h"
#import "CalculatorDoc.h"


@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    CalculatorDoc *calc1 = [[CalculatorDoc alloc] initWithTitle:@"AWG Size Calculator" rvalue:0 thumbImage:[UIImage imageNamed:@"ic_awg.png"]];
    CalculatorDoc *calc2 = [[CalculatorDoc alloc] initWithTitle:@"Battery Calculator" rvalue:1 thumbImage:[UIImage imageNamed:@"ic_battery_life.png"]];
    CalculatorDoc *calc3 = [[CalculatorDoc alloc] initWithTitle:@"Delta-WYE Transformation" rvalue:2 thumbImage:[UIImage imageNamed:@"ic_delta_wye.png"]];
    CalculatorDoc *calc4 = [[CalculatorDoc alloc] initWithTitle:@"Horsepower Calculator" rvalue:3 thumbImage:[UIImage imageNamed:@"ic_hp.png"]];
    CalculatorDoc *calc5 = [[CalculatorDoc alloc] initWithTitle:@"Power Calculator" rvalue:4 thumbImage:[UIImage imageNamed:@"ic_power_triangle.png"]];
    CalculatorDoc *calc6 = [[CalculatorDoc alloc] initWithTitle:@"THD Calculator" rvalue:5 thumbImage:[UIImage imageNamed:@"ic_thd.png"]];
    CalculatorDoc *calc7 = [[CalculatorDoc alloc] initWithTitle:@"Voltage Drop" rvalue:6 thumbImage:[UIImage imageNamed:@"ic_vdrop.png"]];
     CalculatorDoc *calc8 = [[CalculatorDoc alloc] initWithTitle:@"Ampacity Table" rvalue:7 thumbImage:[UIImage imageNamed:@"ic_ampacity"]];
    NSMutableArray *calculators = [NSMutableArray arrayWithObjects:calc1, calc2, calc3, calc4, calc5, calc6, calc7, calc8, nil];

    
    UINavigationController *navController = (UINavigationController *) self.window.rootViewController;
    MasterViewController *masterController = [navController.viewControllers objectAtIndex:0];
    masterController.Calculators = calculators;
    
 

    
    return YES;

    
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - Split view


@end
