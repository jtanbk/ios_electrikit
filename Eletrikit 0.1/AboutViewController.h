//
//  AboutViewController.h
//  Electrikit 0.1
//
//  Created by Jeremiah on 6/27/18.
//  Copyright (c) 2018 Jeremiah. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIButton *WebsiteButton;
@property (weak, nonatomic) IBOutlet UIImage *facebookIG;
@property (weak, nonatomic) IBOutlet UIImage *twitterIG;
@property (weak, nonatomic) IBOutlet UIImage *linkedinIG;
@property (weak, nonatomic) IBOutlet UIImage *googleplusIG;
@property (weak, nonatomic) IBOutlet UIImage *youtubeIG;
@property (weak, nonatomic) IBOutlet UIImage *phoneIG;
@property (weak, nonatomic) IBOutlet UILabel *versionLB;

-(IBAction)websitebuttonTapped:(id)sender;
-(IBAction)facebookTapped:(id)sender;
-(IBAction)twitterTapped:(id)sender;
-(IBAction)linkedinTapped:(id)sender;
-(IBAction)googleplusTapped:(id)sender;
-(IBAction)youtubeTapped:(id)sender;
-(IBAction)phoneTapped:(id)sender;
-(IBAction)reportTapped:(id)sender;

@end
