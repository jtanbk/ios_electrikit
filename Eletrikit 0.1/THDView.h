//
//  THDView.h
//  Electrikit 0.1
//
//  Created by Jeremiah on 7/13/18.
//  Copyright (c) 2018 Jeremiah. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface THDView : UIViewController<UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *fundTF;
@property (weak, nonatomic) IBOutlet UITextField *secondTF;
@property (weak, nonatomic) IBOutlet UITextField *thirdTF;
@property (weak, nonatomic) IBOutlet UITextField *forthTF;
@property (weak, nonatomic) IBOutlet UITextField *fifthTF;
@property (weak, nonatomic) IBOutlet UITextField *sixthTF;
@property (weak, nonatomic) IBOutlet UITextField *seventhTF;
@property (weak, nonatomic) IBOutlet UITextField *eighthTF;
@property (weak, nonatomic) IBOutlet UITextField *ninthTF;
@property (weak, nonatomic) IBOutlet UITextField *tenthTF;
@property (weak, nonatomic) IBOutlet UITextField *eleventhTF;
@property (weak, nonatomic) IBOutlet UITextField *twelvethTF;
@property (weak, nonatomic) IBOutlet UITextField *thirteenthTF;
@property (weak, nonatomic) IBOutlet UITextField *fourteenthTF;
@property (weak, nonatomic) IBOutlet UITextField *fifteenthTF;
@property (weak, nonatomic) IBOutlet UITextField *sixteenthTF;
@property (weak, nonatomic) IBOutlet UITextField *seventeenthTF;
@property (weak, nonatomic) IBOutlet UITextField *eightteenthTF;
@property (weak, nonatomic) IBOutlet UITextField *nineteenthTF;
@property (weak, nonatomic) IBOutlet UITextField *twentythTF;
@property (weak, nonatomic) IBOutlet UITextField *thdTF;


@property (strong, nonatomic) IBOutlet UIPickerView *typePK;
@property (strong, nonatomic) IBOutlet UIPickerView *unitPK;

@property (weak, nonatomic) IBOutlet UILabel *unitLB;
@property (weak, nonatomic) IBOutlet UILabel *harmonicsLB;

-(IBAction)pressCalc:(id)sender;



@end
