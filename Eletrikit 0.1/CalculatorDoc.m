//
//  CalculatorDoc.m
//  Electrikit 0.1
//
//  Created by Jeremiah on 6/26/18.
//  Copyright (c) 2018 Jeremiah. All rights reserved.
//
#import "CalculatorInfo.h"
#import "CalculatorDoc.h"

@implementation CalculatorDoc
@synthesize data = _data;
@synthesize thumbImage = _thumbImage;



- (id)initWithTitle:(NSString*)title rvalue:(float)rvalue thumbImage:(UIImage *)thumbImage  {
    if ((self = [super init])) {
        self.data = [[CalculatorInfo alloc] initWithTitle:title rvalue:rvalue];
        self.thumbImage = thumbImage;
    }
    return self;
}

@end
