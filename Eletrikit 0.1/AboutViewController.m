//
//  AboutViewController.m
//  Electrikit 0.1
//
//  Created by Jeremiah on 6/27/18.
//  Copyright (c) 2018 Jeremiah. All rights reserved.
//

#import "AboutViewController.h"

@interface AboutViewController ()

@end

@implementation AboutViewController
@synthesize versionLB;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
   // NSString *wiresizeareaString =[NSString stringWithFormat:@"Area: %1.4f in^2, %1.4f mm^2, %1.4f kcmil", inTwo, mmTwo, kcm];
    NSMutableString *build = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleVersionKey];
    NSMutableString *version = [NSMutableString stringWithString:@"Version: "];
    [version appendString:build];
    versionLB.text = version;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)websitebuttonTapped:(id)sender
{
    NSURL *url = [ [ NSURL alloc ] initWithString: @"http://www.bkprecision.com/" ];
    [[UIApplication sharedApplication] openURL:url];
    
    
  
}



- (IBAction)youtubeTapped:(id)sender
{
    NSURL *url = [ [ NSURL alloc ] initWithString: @"https://www.youtube.com/channel/UCeTANSW-8-RhIpNLn-jtMcg" ];
    [[UIApplication sharedApplication] openURL:url];
    
}


- (IBAction)twitterTapped:(id)sender
{
    NSURL *url = [ [ NSURL alloc ] initWithString: @"https://twitter.com/bkprecision" ];
    [[UIApplication sharedApplication] openURL:url];
    
    
}


- (IBAction)googleplusTapped:(id)sender
{
    NSURL *url = [ [ NSURL alloc ] initWithString: @"https://plus.google.com/+Bkprecision1" ];
    [[UIApplication sharedApplication] openURL:url];
    
    
}


- (IBAction)linkedinTapped:(id)sender
{
    NSURL *url = [ [ NSURL alloc ] initWithString: @"https://www.linkedin.com/company/b&k-precision-corporation" ];
    [[UIApplication sharedApplication] openURL:url];
    
    
}

- (IBAction)facebookTapped:(id)sender
{
    NSURL *url = [ [ NSURL alloc ] initWithString: @"https://www.facebook.com/BKPrecisionInc" ];
    [[UIApplication sharedApplication] openURL:url];
    
}

- (IBAction)phoneTapped:(id)sender
{
    NSURL *url = [ [ NSURL alloc ] initWithString: @"tel:7149219095" ];
    [[UIApplication sharedApplication] openURL:url];
    
    
    
}


- (IBAction)reportTapped:(id)sender
{
    NSURL *url = [ [ NSURL alloc ] initWithString: @"http://www.bkprecision.com/contact-us.html" ];
    [[UIApplication sharedApplication] openURL:url];
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
