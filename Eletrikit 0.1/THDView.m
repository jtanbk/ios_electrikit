//
//  THDView.m
//  Electrikit 0.1
//
//  Created by Jeremiah on 7/13/18.
//  Copyright (c) 2018 Jeremiah. All rights reserved.
//

#import "THDView.h"

@interface THDView ()

@end

@implementation THDView
@synthesize fundTF, secondTF, thirdTF, forthTF, fifthTF, sixthTF, seventhTF, eighthTF, ninthTF, tenthTF, eleventhTF, twelvethTF, thirteenthTF, fourteenthTF, fifteenthTF, sixteenthTF, seventeenthTF, eightteenthTF, nineteenthTF, twentythTF, thdTF, typePK, unitPK, unitLB, harmonicsLB;

int unitSet = 0;
int typeSet = 0;

int eighthTF_Set, ninethTF_Set, tenthTF_Set, eighteenthTF_Set, nineteenthTF_Set, twentythTF_Set;
double thdNum;
double percentthdNum;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"THD Calculator";
    
    typePK.tag = 0;
    typePK.delegate = self;
    typePK.dataSource = self;
    typePK.showsSelectionIndicator = YES;
    [self.view addSubview:typePK];
    unitPK.tag = 1;
    unitPK.delegate = self;
    unitPK.dataSource = self;
    unitPK.showsSelectionIndicator = YES;
    [self.view addSubview:unitPK];
    eighthTF.delegate = self;
    eighthTF.tag = 0;
    ninthTF.delegate = self;
    ninthTF.tag = 1;
    tenthTF.delegate = self;
    tenthTF.tag = 2;
    eightteenthTF.delegate = self;
    eightteenthTF.tag = 3;
    nineteenthTF.delegate = self;
    nineteenthTF.tag = 4;
    twentythTF.delegate = self;
    twentythTF.tag = 5;
    
    
    thdTF.userInteractionEnabled = NO;
    /*
    //Adjust picker size
    CGAffineTransform b0 = CGAffineTransformMakeTranslation (0, typePK.bounds.size.height/1.5);
    CGAffineTransform c0 = CGAffineTransformMakeScale       (1, 0.4);
    CGAffineTransform b1 = CGAffineTransformMakeTranslation (0, -typePK.bounds.size.height/1.5);
    typePK.transform = CGAffineTransformConcat          (b0, CGAffineTransformConcat(c0, b1));
    
    
    CGAffineTransform t0 = CGAffineTransformMakeTranslation (0, unitPK.bounds.size.height/1.5);
    CGAffineTransform s0 = CGAffineTransformMakeScale       (1, 0.4);
    CGAffineTransform t1 = CGAffineTransformMakeTranslation (0, -unitPK.bounds.size.height/1.5);
    unitPK.transform = CGAffineTransformConcat          (t0, CGAffineTransformConcat(s0, t1));
    */
    
    CGRect frame = typePK.frame;
    frame.size.width = 95; // width to be displayed on popover controller
    typePK.frame = frame;
    frame = unitPK.frame;
    frame.size.width = 95; // width to be displayed on popover controller
    unitPK.frame = frame;
    
    
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component{
    switch (component){
        case 0:
            return 90.0f;
        case 1:
            return 70.0f;
    }
    return 0;
}

//SET UP PICKER TITLES
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    if (pickerView.tag == 0){
        NSString * t_title = nil;
        switch(row) {
            case 0:
                t_title = @"Voltage";
                break;
            case 1:
                t_title = @"Power";
                break;
        }
        return t_title;
    }

    if (pickerView.tag == 1){
            NSString * u_title = nil;
            switch(row) {
                case 0:
                    u_title = @"RMS";
                    break;
                case 1:
                    u_title = @"Peak";
                    break;
            }
        return u_title;

    }
    return  0;
}


//Setting conditions with picker  
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    if (pickerView.tag == 0){
        switch(row) {
            case 0:
                typeSet = 0;
                unitPK.hidden = NO;
                unitLB.hidden = NO;
                harmonicsLB.text = @"Harmonics(in Vrms):";
                break;
            case 1:
                typeSet = 1;
                unitPK.hidden = YES;
                unitLB.hidden = YES;
                harmonicsLB.text = @"Harmonics(in W):";
                break;
        }
    
    }else if (pickerView.tag == 1){
        switch(row) {
            case 0:
                unitSet = 0;
                harmonicsLB.text = @"Harmonics(in Vrms):";
                break;
            case 1:
                unitSet = 1;
                harmonicsLB.text = @"Harmonics(in Vpk):";
                break;
        }
    }


}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
        return 2;
    
}


-(IBAction)pressCalc:(id)sender{
    //Decimal Formatting
    NSNumberFormatter *df3 = [[NSNumberFormatter alloc] init];
    df3.decimalSeparator = @".";
    df3.minimumFractionDigits = 1;
    df3.maximumFractionDigits = 3;
    [df3 setRoundingMode: NSNumberFormatterRoundUp];
    
    percentthdNum = 0;
    thdNum = 0;
    
    //Storing all values into an array
    double h[20];
    h[0] = [fundTF.text floatValue];
    h[1] = [secondTF.text floatValue];
    h[2] = [thirdTF.text floatValue];
    h[3] = [forthTF.text floatValue];
    h[4] = [fifthTF.text floatValue];
    h[5] = [sixthTF.text floatValue];
    h[6] = [seventhTF.text floatValue];
    h[7] = [eighthTF.text floatValue];
    h[8] = [ninthTF.text floatValue];
    h[9] = [tenthTF.text floatValue];
    h[10] = [eleventhTF.text floatValue];
    h[11] = [twelvethTF.text floatValue];
    h[12] = [thirteenthTF.text floatValue];
    h[13] = [fourteenthTF.text floatValue];
    h[14] = [fifteenthTF.text floatValue];
    h[15] = [sixteenthTF.text floatValue];
    h[16] = [seventeenthTF.text floatValue];
    h[17] = [eightteenthTF.text floatValue];
    h[18] = [nineteenthTF.text floatValue];
    h[19] = [twentythTF.text floatValue];
    
   
    
    if (typeSet == 0 && unitSet == 0){
        //Voltage and RMS pick
        for (int n = 1;n < 20; n++){
            thdNum += h[n] * h[n];   //compute numerator
        }
        
        percentthdNum = 100 * sqrt(thdNum / h[0]);
        
        NSString *percentthdString = [df3 stringFromNumber:[NSNumber numberWithDouble:percentthdNum]];
        thdTF.text = percentthdString;

    }else if(typeSet == 0 && unitSet == 1){
        for (int n = 1;n < 20; n++){
            thdNum += pow((h[n] / sqrt(2)),2);   //compute numerator
        }
            percentthdNum = 100 * sqrt(thdNum / h[0]);
        
        NSString *percentthdString = [df3 stringFromNumber:[NSNumber numberWithDouble:percentthdNum]];
        thdTF.text = percentthdString;

    }else if(typeSet == 1){
        for (int n = 1;n < 20; n++){
            thdNum += h[n];      //compute numerator
        }
        percentthdNum = 100 * sqrt(thdNum / h[0]);
        
        NSString *percentthdString = [df3 stringFromNumber:[NSNumber numberWithDouble:percentthdNum]];
        thdTF.text = percentthdString;
    }

}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//Move screen up if editing these fields
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if (textField.tag >= 0 && textField.tag <= 6 ){
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
        eighthTF_Set = 1;
        eighteenthTF_Set = 1;
        ninethTF_Set = 1;
        nineteenthTF_Set = 1;
        tenthTF_Set = 1;
        twentythTF_Set = 1;
        return YES;
    }else{
        return NO;
    }
    
}


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    [self.view endEditing:YES];
    eighthTF_Set = 0;
    eighteenthTF_Set = 0;
    ninethTF_Set = 0;
    nineteenthTF_Set = 0;
    tenthTF_Set = 0;
    twentythTF_Set = 0;
    return YES;
}




- (void)keyboardDidShow:(NSNotification *)notification
{
    
    // Assign new frame to your view
    if (eighthTF_Set == 1 || eighteenthTF_Set == 1 || ninethTF_Set == 1 || nineteenthTF_Set == 1 || tenthTF_Set == 1 || twentythTF_Set || 1){
        [self.view setFrame:CGRectMake(0,-110,self.view.frame.size.width,self.view.frame.size.height)]; //
    }else{
        [self.view setFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)]; //here
    }
}





-(void)keyboardDidHide:(NSNotification *)notification
{
    [self.view setFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
