//
//  HorsepowerView.m
//  Electrikit 0.1
//
//  Created by Jeremiah on 7/10/18.
//  Copyright (c) 2018 Jeremiah. All rights reserved.
//

#import "HorsepowerView.h"

@interface HorsepowerView ()

@end

@implementation HorsepowerView

@synthesize horsepowerPK, horsepowerTF, horsepowerAV, voltageTF, currentTF, kilovoltTF, kilowattsTF, powerfactorTF, efficiencyTF, kilovoltBT, kilovoltLB, kilovoltLB2, powerfactorLB;

int horsepowerSet, efficiencytfSet, horsepowertfSet;
double factor;
double factorPF;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
     self.title = @"Horsepower Calculator";
    
    //Adjust picker size
    horsepowerPK.delegate = self;
    horsepowerPK.dataSource = self;
    horsepowerPK.showsSelectionIndicator = YES;
    [self.view addSubview:horsepowerPK];
    
    //Setting tags
    horsepowerTF.tag = 0;
    voltageTF.tag = 1;
    currentTF.tag = 2;
    kilowattsTF.tag = 3;
    kilovoltTF.tag = 4;
    powerfactorTF.tag = 5;
    efficiencyTF.tag = 6;
    
    horsepowerTF.delegate = self;
    voltageTF.delegate = self;
    currentTF.delegate = self;
    kilowattsTF.delegate = self;
    kilovoltTF.delegate = self;
    powerfactorTF.delegate = self;
    efficiencyTF.delegate = self;
    
    [self.view addSubview:horsepowerTF];
    [self.view addSubview:voltageTF];
    [self.view addSubview:currentTF];
    [self.view addSubview:kilowattsTF];
    [self.view addSubview:kilovoltTF];
    [self.view addSubview:powerfactorTF];
    [self.view addSubview:efficiencyTF];
    
    
    factor = 1;
 
}

//Horsepower drop down list
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    NSString * title = nil;
        switch(row) {
        case 0:
            title = @"AC-Single Phase";
                
            break;
        case 1:
            title = @"AC-Two Phase 4-wire";
            
            break;
        case 2:
            title = @"AC-Three Phase";
                
            break;
        case 3:
            title = @"DC";
               
            break;
    }
    return title;
}


//Set factor and factorPF via picker
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    float voltageNum = [voltageTF.text floatValue];
    float currentNum = [currentTF.text floatValue];
    float efficiencyNum = [efficiencyTF.text floatValue];
    float horsepowerNum = [horsepowerTF.text floatValue];
    float pfNum = [powerfactorTF.text floatValue];
    
    NSNumberFormatter *df3 = [[NSNumberFormatter alloc] init];
    df3.decimalSeparator = @".";
    df3.minimumFractionDigits = 1;
    df3.maximumFractionDigits = 3;
    
    switch(row) {
        case 0:{
            horsepowerSet = 0;
            factor = 1;
            horsepowerNum = (voltageNum * currentNum * factor * pfNum * efficiencyNum) / 746;
            NSString *horsepowerString = [df3 stringFromNumber:[NSNumber numberWithDouble:horsepowerNum]];
            horsepowerTF.text = horsepowerString;
            double KW = ((voltageNum * currentNum * pfNum * factor) / 1000);
            double KVA = ((voltageNum * currentNum * factor) / 1000);
            NSString *hpkwString = [df3 stringFromNumber:[NSNumber numberWithDouble:KW]];
            kilowattsTF.text = hpkwString;
            NSString *hpkvaString = [df3 stringFromNumber:[NSNumber numberWithDouble:KVA]];
            kilovoltTF.text = hpkvaString;
            
            
            kilovoltTF.hidden = NO;
            kilovoltLB.hidden = NO;
            kilovoltLB2.hidden = NO;
            kilovoltBT.hidden = NO;
            powerfactorLB.hidden = NO;
            powerfactorTF.hidden = NO;
            break;
        }
        case 1:{
            horsepowerSet = 1;
            factor = 2;
            horsepowerNum = (voltageNum * currentNum * factor * pfNum * efficiencyNum) / 746;
            NSString *horsepowerString = [df3 stringFromNumber:[NSNumber numberWithDouble:horsepowerNum]];
            horsepowerTF.text = horsepowerString;
            double KW = ((voltageNum * currentNum * pfNum * factor) / 1000);
            double KVA = ((voltageNum * currentNum * factor) / 1000);
            NSString *hpkwString = [df3 stringFromNumber:[NSNumber numberWithDouble:KW]];
            kilowattsTF.text = hpkwString;
            NSString *hpkvaString = [df3 stringFromNumber:[NSNumber numberWithDouble:KVA]];
            kilovoltTF.text = hpkvaString;
            
            kilovoltTF.hidden = NO;
            kilovoltLB.hidden = NO;
            kilovoltLB2.hidden = NO;
            kilovoltBT.hidden = NO;
            powerfactorLB.hidden = NO;
            powerfactorTF.hidden = NO;
            break;
            
        }
        case 2:{
            horsepowerSet = 2;
            factor = 1.73;
            horsepowerNum = (voltageNum * currentNum * factor * pfNum * efficiencyNum) / 746;
            NSString *horsepowerString = [df3 stringFromNumber:[NSNumber numberWithDouble:horsepowerNum]];
            horsepowerTF.text = horsepowerString;
            double KW = ((voltageNum * currentNum * pfNum * factor) / 1000);
            double KVA = ((voltageNum * currentNum * factor) / 1000);
            NSString *hpkwString = [df3 stringFromNumber:[NSNumber numberWithDouble:KW]];
            kilowattsTF.text = hpkwString;
            NSString *hpkvaString = [df3 stringFromNumber:[NSNumber numberWithDouble:KVA]];
            kilovoltTF.text = hpkvaString;
            
            kilovoltTF.hidden = NO;
            kilovoltLB.hidden = NO;
            kilovoltLB2.hidden = NO;
            kilovoltBT.hidden = NO;
            powerfactorLB.hidden = NO;
            powerfactorTF.hidden = NO;
            break;
        }
        case 3:{
            horsepowerSet = 3;
            factorPF = 1;
            horsepowerNum = (voltageNum * currentNum * efficiencyNum) / 746;
            NSString *horsepowerString = [df3 stringFromNumber:[NSNumber numberWithDouble:horsepowerNum]];
            horsepowerTF.text = horsepowerString;
            double KW = ((voltageNum * currentNum) / 1000);
           
            NSString *hpkwString = [df3 stringFromNumber:[NSNumber numberWithDouble:KW]];
            kilowattsTF.text = hpkwString;
        
            
            kilovoltTF.hidden = YES;
            kilovoltLB.hidden = YES;
            kilovoltLB2.hidden = YES;
            kilovoltBT.hidden = YES;
            powerfactorLB.hidden = YES;
            powerfactorTF.hidden = YES;
            break;
        }
    }
}


- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return 4;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}


   - (void)textFieldDidEndEditing:(UITextField *)textfield {
       //Decimal Formatting
       NSNumberFormatter *df3 = [[NSNumberFormatter alloc] init];
       df3.decimalSeparator = @".";
       df3.minimumFractionDigits = 1;
       df3.maximumFractionDigits = 3;
       
       float pfNum = [powerfactorTF.text floatValue];
       double pf;
       //Display alert if power factor is not in range // only range 0-1
       NSString *title = @"Check Submitted Values";
       NSString *message = @"The range in Power Factor can only be from 0 to 1";
       NSString *okText = @"OK";
       if (pfNum > 1 || pfNum < 0){
           UIAlertController * alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
           UIAlertAction *okButton  = [UIAlertAction actionWithTitle:okText style:UIAlertActionStyleCancel handler:nil];
           [alert addAction:okButton];
           [self presentViewController:alert animated:YES completion:nil];
           pf = 1;
           NSString *pfString = [df3 stringFromNumber:[NSNumber numberWithDouble:pf]];
           powerfactorTF.text = pfString;
       }
       
       
       
      //  NSLog(@"Int value is %f %f", factorPF, factor);
       
       /*
       
    //Selecting factor and factorPF based on picker
    pfNum = [powerfactorTF.text floatValue];
       if(horsepowerSet == 0){
           factor = 1;
           factorPF = factor * pfNum;
       }else if(horsepowerSet == 1){
           factor = 2;
           factorPF = factor * pfNum;
       }else if(horsepowerSet == 2){
           factor = 1.73;
           factorPF = factor * pfNum;
       }else if (horsepowerSet == 3){
           factorPF = 1;
       }
        
        */
       
       /*
        //Setting tags
        horsepowerTF.delegate = self;
        horsepowerTF.tag = 0;
        voltageTF.tag = 1;
        currentTF.tag = 2;
        kilowattsTF.tag = 3;
        kilovoltTF.tag = 4;
        powerfactorTF.tag = 5;
        efficiencyTF.delegate = self;
        efficiencyTF.tag = 6;
        */

     
      
       
       //Voltage edited update Kilowatts and Kilovolt and horsepower
       if(textfield.tag == 1){

           float voltageNum = [voltageTF.text floatValue];
           float currentNum = [currentTF.text floatValue];;
           float efficiencyNum = [efficiencyTF.text floatValue];
           float horsepowerNum = [horsepowerTF.text floatValue];
           
        
           double vKW, vKVA;
        
           if (horsepowerSet == 3){
               pfNum = 1;
           }
           
           vKW = (voltageNum * currentNum * pfNum * factor) / 1000;
           NSString *vkwString = [df3 stringFromNumber:[NSNumber numberWithDouble:vKW]];
           kilowattsTF.text = vkwString;
           vKVA = (voltageNum * currentNum * factor) / 1000;
           NSString *vkvaString = [df3 stringFromNumber:[NSNumber numberWithDouble:vKVA]];
           kilovoltTF.text = vkvaString;
           
           // Calculate horsepower here

           horsepowerNum = (voltageNum * currentNum * factor * pfNum * efficiencyNum) / 746;
           NSString *horsepowerString = [df3 stringFromNumber:[NSNumber numberWithDouble:horsepowerNum]];
           horsepowerTF.text = horsepowerString;
           
           
       }
       //Current edited update kilowatts and kilovolts and horsepower
       if(textfield.tag == 2){

           float voltageNum = [voltageTF.text floatValue];
           float currentNum = [currentTF.text floatValue];
           float efficiencyNum = [efficiencyTF.text floatValue];
           float horsepowerNum = [horsepowerTF.text floatValue];
           
           if (horsepowerSet == 3){
               pfNum = 1;
           }
           
       
           double vKW, vKVA;
           vKW = (voltageNum * currentNum * pfNum * factor) / 1000;
           NSString *vkwString = [df3 stringFromNumber:[NSNumber numberWithDouble:vKW]];
           kilowattsTF.text = vkwString;
           vKVA = (voltageNum * currentNum * factor) / 1000;
           NSString *vkvaString = [df3 stringFromNumber:[NSNumber numberWithDouble:vKVA]];
           kilovoltTF.text = vkvaString;
           
      
           // Calculate horsepower here
           horsepowerNum = (voltageNum * currentNum * pfNum * factor * efficiencyNum) / 746;
           NSString *horsepowerString = [df3 stringFromNumber:[NSNumber numberWithDouble:horsepowerNum]];
           horsepowerTF.text = horsepowerString;
           
       }
       //Kilowatts edited
       if(textfield.tag ==3){
           //when kilowatt changes update current and kilovolt and Horsepower
           float voltageNum = [voltageTF.text floatValue];
           float kilowattNum = [kilowattsTF.text floatValue];
           float horsepowerNum = [horsepowerTF.text floatValue];
           float efficiencyNum = [efficiencyTF.text floatValue];
           float currentNum = [currentTF.text floatValue];
           double kwC, kwKVA;
           
           //Single Phase
           if(horsepowerSet == 0){
           kwC = ((kilowattNum * 1000) / (voltageNum * pfNum));
           NSString *kwcString = [df3 stringFromNumber:[NSNumber numberWithDouble:kwC]];
           currentTF.text = kwcString;
           float currentNum = [currentTF.text floatValue];
           kwKVA = ((voltageNum * currentNum) / 1000);
           NSString * kwkvaString = [df3 stringFromNumber:[NSNumber numberWithDouble:kwKVA]];
           kilovoltTF.text = kwkvaString;
               
            //Two Phase
           }else if(horsepowerSet == 1){
               kwC = ((kilowattNum * 1000) / (voltageNum * factor * efficiencyNum));
               NSString *kwcString = [df3 stringFromNumber:[NSNumber numberWithDouble:kwC]];
               currentTF.text = kwcString;
               float currentNum = [currentTF.text floatValue];
               kwKVA = ((factor * voltageNum * currentNum) / 1000);
               NSString * kwkvaString = [df3 stringFromNumber:[NSNumber numberWithDouble:kwKVA]];
               kilovoltTF.text = kwkvaString;
               
            //Three phase
           }else if (horsepowerSet == 2){
               kwC = ((kilowattNum * 1000) / (voltageNum * factor * efficiencyNum));
               NSString *kwcString = [df3 stringFromNumber:[NSNumber numberWithDouble:kwC]];
               currentTF.text = kwcString;
               float currentNum = [currentTF.text floatValue];
               kwKVA = ((factor * voltageNum * currentNum) / 1000);
               NSString * kwkvaString = [df3 stringFromNumber:[NSNumber numberWithDouble:kwKVA]];
               kilovoltTF.text = kwkvaString;
               //DC
           } else if (horsepowerSet == 3){
               kwC = ((kilowattNum * 1000) / (voltageNum));
               NSString *kwcString = [df3 stringFromNumber:[NSNumber numberWithDouble:kwC]];
               currentTF.text = kwcString;
               pfNum = 1;
           }
           
             // NSLog(@"Int value is %f %f", factorPF, pfNum);
           // Calculate horsepower here
           currentNum = [currentTF.text floatValue];
           horsepowerNum = (voltageNum * currentNum * factor * pfNum * efficiencyNum) / 746;
           NSString *horsepowerString = [df3 stringFromNumber:[NSNumber numberWithDouble:horsepowerNum]];
           horsepowerTF.text = horsepowerString;
       
       }
       //Kilovolts edited update kilowatts and current and horsepower
       if(textfield.tag == 4){
           float voltageNum = [voltageTF.text floatValue];
           float kilovaNum = [kilovoltTF.text floatValue];
           float horsepowerNum = [horsepowerTF.text floatValue];
           float efficiencyNum = [efficiencyTF.text floatValue];
           float pfNum = [powerfactorTF.text floatValue];
            float currentNum = [currentTF.text floatValue];
          double kvaC, kvaKW;
           
           //Single Phase
           if(horsepowerSet == 0){
               
               kvaC = ((kilovaNum * 1000) / (voltageNum * factor));
               NSString *kwcString = [df3 stringFromNumber:[NSNumber numberWithDouble:kvaC]];
               currentTF.text = kwcString;
               
              
               kvaKW = ((voltageNum * currentNum * pfNum) / 1000);
               NSString * kwkvaString = [df3 stringFromNumber:[NSNumber numberWithDouble:kvaKW]];
               kilowattsTF.text = kwkvaString;
            
               //Two Phase
           }else if(horsepowerSet == 1){
              
               kvaC = ((kilovaNum * 1000) / (voltageNum * factor));
               NSString *kwcString = [df3 stringFromNumber:[NSNumber numberWithDouble:kvaC]];
               currentTF.text = kwcString;

               kvaKW = ((voltageNum * currentNum * pfNum) / 1000);
               NSString * kwkvaString = [df3 stringFromNumber:[NSNumber numberWithDouble:kvaKW]];
               kilowattsTF.text = kwkvaString;
               
               
               //Three phase
           }else if (horsepowerSet == 2){
             
               kvaC = ((kilovaNum * 1000) / (voltageNum * factor));
               NSString *kwcString = [df3 stringFromNumber:[NSNumber numberWithDouble:kvaC]];
               currentTF.text = kwcString;
              
               kvaKW = ((voltageNum * currentNum * pfNum * factor) / 1000);
               NSString * kwkvaString = [df3 stringFromNumber:[NSNumber numberWithDouble:kvaKW]];
               kilowattsTF.text = kwkvaString;
               //DC hides Kilovolts
           }else if (horsepowerSet == 3){
            
               
           }
           
       
        
             currentNum = [currentTF.text floatValue];
           // Calculate horsepower here
        
           horsepowerNum = (voltageNum * currentNum * factor * pfNum * efficiencyNum) / 746;
           NSString *horsepowerString = [df3 stringFromNumber:[NSNumber numberWithDouble:horsepowerNum]];
           horsepowerTF.text = horsepowerString;
       }
       //Powerfactor  Edited update kilowatts and horsepower
       if(textfield.tag == 5){
           float voltageNum = [voltageTF.text floatValue];
           float horsepowerNum = [horsepowerTF.text floatValue];
           float efficiencyNum = [efficiencyTF.text floatValue];
           float currentNum = [currentTF.text floatValue];
           float pfNum = [powerfactorTF.text floatValue];
           
           //Calculate KW
           double kw;
           kw = ((voltageNum * currentNum * pfNum * factor) / 1000);
           NSString * kwString = [df3 stringFromNumber:[NSNumber numberWithDouble:kw]];
           kilowattsTF.text = kwString;
           
           // Calculate horsepower here
           factorPF = factor * pfNum;
           horsepowerNum = (voltageNum * currentNum * factorPF * efficiencyNum) / 746;
           NSString *horsepowerString = [df3 stringFromNumber:[NSNumber numberWithDouble:horsepowerNum]];
           horsepowerTF.text = horsepowerString;
           
       }
       //Effiency edited update horsepower again
       if(textfield.tag == 6){
           float voltageNum = [voltageTF.text floatValue];
           float horsepowerNum = [horsepowerTF.text floatValue];
           float efficiencyNum = [efficiencyTF.text floatValue];
           float currentNum = [currentTF.text floatValue];
           
           // Calculate horsepower here
           horsepowerNum = (voltageNum * currentNum * factorPF * efficiencyNum) / 746;
           NSString *horsepowerString = [df3 stringFromNumber:[NSNumber numberWithDouble:horsepowerNum]];
           horsepowerTF.text = horsepowerString;
       }
       //Horsepower edited update kilowatts current kilowatts and kilovolts
       if(textfield.tag == 0){
           float voltageNum = [voltageTF.text floatValue];
        
           float efficiencyNum = [efficiencyTF.text floatValue];
           float pfNum = [powerfactorTF.text floatValue];
           float horsepowerNum = [horsepowerTF.text floatValue];
           
           if(horsepowerSet == 0){
               double hpC, hpKW, hpKVA;
               hpC = (horsepowerNum * 746 ) / (voltageNum * efficiencyNum * pfNum);
               NSString *hpcString = [df3 stringFromNumber:[NSNumber numberWithDouble:hpC]];
               currentTF.text = hpcString;
               float currentNum = [currentTF.text floatValue];
               hpKW = ((voltageNum * currentNum * pfNum) / 1000);
               hpKVA = ((voltageNum * currentNum) / 1000);
               NSString *hpkwString = [df3 stringFromNumber:[NSNumber numberWithDouble:hpKW]];
               kilowattsTF.text = hpkwString;
               NSString *hpkvaString = [df3 stringFromNumber:[NSNumber numberWithDouble:hpKVA]];
               kilovoltTF.text = hpkvaString;
            
               
               //Two Phase
           }else if(horsepowerSet == 1){
               double hpC, hpKW, hpKVA;
               hpC = (horsepowerNum * 746 ) / (voltageNum * efficiencyNum * pfNum * factor);
               NSString *hpcString = [df3 stringFromNumber:[NSNumber numberWithDouble:hpC]];
               currentTF.text = hpcString;
               float currentNum = [currentTF.text floatValue];
               hpKW = ((voltageNum * currentNum * pfNum * factor) / 1000);
               hpKVA = ((voltageNum * currentNum * factor) / 1000);
               NSString *hpkwString = [df3 stringFromNumber:[NSNumber numberWithDouble:hpKW]];
               kilowattsTF.text = hpkwString;
               NSString *hpkvaString = [df3 stringFromNumber:[NSNumber numberWithDouble:hpKVA]];
               kilovoltTF.text = hpkvaString;
              
               
               
               //Three phase
           }else if (horsepowerSet == 2){
               double hpC, hpKW, hpKVA;
               hpC = (horsepowerNum * 746 ) / (voltageNum * efficiencyNum * pfNum * factor);
               NSString *hpcString = [df3 stringFromNumber:[NSNumber numberWithDouble:hpC]];
               currentTF.text = hpcString;
               float currentNum = [currentTF.text floatValue];
               hpKW = ((voltageNum * currentNum * pfNum * factor) / 1000);
               hpKVA = ((voltageNum * currentNum * factor) / 1000);
               NSString *hpkwString = [df3 stringFromNumber:[NSNumber numberWithDouble:hpKW]];
               kilowattsTF.text = hpkwString;
               NSString *hpkvaString = [df3 stringFromNumber:[NSNumber numberWithDouble:hpKVA]];
               kilovoltTF.text = hpkvaString;
               
         
               //DC
           }else if (horsepowerSet == 3){
               double hpC;
               hpC = (horsepowerNum * 746 ) / (voltageNum * efficiencyNum);
               NSString *hpcString = [df3 stringFromNumber:[NSNumber numberWithDouble:hpC]];
               currentTF.text = hpcString;
           }
           
       }
   }
       
   
   
//If horsepower and Effiency are being edited then move the screen up
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if (textField.tag == 0 || textField.tag == 6){
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
        efficiencytfSet = 1;
        horsepowertfSet = 1;
        return TRUE;
    }else if (textField.tag >= 1 || textField.tag < 6 ){
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
        return TRUE;
    }{
        return FALSE;
    }
    
}


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    [self.view endEditing:YES];
    horsepowertfSet = 0;
    efficiencytfSet = 0;
    return YES;
}




- (void)keyboardDidShow:(NSNotification *)notification
{
    
    // Assign new frame to your view
    if (horsepowertfSet == 1 || efficiencytfSet == 1){
        [self.view setFrame:CGRectMake(0,-110,self.view.frame.size.width,self.view.frame.size.height)]; //here taken -110 for example i.e. your view will be scrolled to -110. change its value according to your requirement.
    }else{
        [self.view setFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)]; //here taken -110 for example i.e. your view will be scrolled to -110. change its value according to your requirement.
    }
}




//Move back 
-(void)keyboardDidHide:(NSNotification *)notification
{
    [self.view setFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
}



-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
