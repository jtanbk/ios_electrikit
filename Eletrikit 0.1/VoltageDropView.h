//
//  VoltageDropView.h
//  Electrikit 0.1
//
//  Created by Jeremiah on 7/16/18.
//  Copyright (c) 2018 Jeremiah. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VoltageDropView : UIViewController<UIPickerViewDelegate, UIPickerViewDataSource>

@property (weak, nonatomic) IBOutlet UITextField *voltageTF;
@property (weak, nonatomic) IBOutlet UITextField *currentTF;
@property (weak, nonatomic) IBOutlet UITextField *wirelengthTF;
@property (weak, nonatomic) IBOutlet UITextField *voltagedropTF;
@property (weak, nonatomic) IBOutlet UITextField *percentdropTF;
@property (weak, nonatomic) IBOutlet UITextField *voltageloadTF;

@property (strong, nonatomic) IBOutlet UIPickerView *materialPK;
@property (strong, nonatomic) IBOutlet UIPickerView *wiresizePK;
@property (strong, nonatomic) IBOutlet UIPickerView *voltagetypePK;
@property (strong, nonatomic) IBOutlet UIPickerView *wirelengthPK;

@property (weak, nonatomic) IBOutlet UILabel *wiresizediameterLB;
@property (weak, nonatomic) IBOutlet UILabel *wiresizeareaLB;


-(IBAction)pressCalc:(id)sender;

@end
