//
//  DeltaView.m
//  Electrikit 0.1
//
//  Created by Jeremiah on 6/27/18.
//  Copyright (c) 2018 Jeremiah. All rights reserved.
//

#import "DeltaView.h"

@interface DeltaView ()

@end

@implementation DeltaView

@synthesize raTF, rbTF, rcTF, r1TF,r2TF,r3TF;

int r3TFset = 0;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
      self.title = @"Delta-Wye Transformation";
    
    r3TF.delegate = self;
    r2TF.delegate = self;
    r1TF.delegate = self;
    rcTF.delegate = self;
    rbTF.delegate = self;
    raTF.delegate = self;
    
    
    r3TF.tag = 0;
    r2TF.tag = 1;
    r1TF.tag = 2;
    
    rcTF.tag = 3;
    rbTF.tag = 4;
    raTF.tag = 5;
    
    [self.view addSubview:r3TF];
    [self.view addSubview:r2TF];
    [self.view addSubview:r1TF];
    [self.view addSubview:rcTF];
    [self.view addSubview:rbTF];
    [self.view addSubview:raTF];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (void)textFieldDidEndEditing:(UITextField *)textfield {
    //Decimal Formatting
    NSNumberFormatter *df3 = [[NSNumberFormatter alloc] init];
    df3.decimalSeparator = @".";
    df3.minimumFractionDigits = 1;
    df3.maximumFractionDigits = 3;

    NSNumberFormatter *df3UP = [[NSNumberFormatter alloc] init];
    df3UP.decimalSeparator = @".";
    df3UP.minimumFractionDigits = 1;
    df3UP.maximumFractionDigits = 3;
    [df3UP setRoundingMode: NSNumberFormatterRoundUp];
    
    
    
    if (textfield.tag == 0 || textfield.tag == 1 || textfield.tag == 2){
    //WYE to Delta
    //store all the values from the textfields
    float r1Num = [r1TF.text floatValue];
    float r2Num = [r2TF.text floatValue];
    float r3Num = [r3TF.text floatValue];
    float raNum, rbNum, rcNum;
        
    
    //Calculations
    double rP = (r1Num * r2Num) + (r2Num * r3Num) + (r3Num * r1Num);
    raNum = rP /r1Num;
    rbNum = rP /r2Num;
    rcNum = rP /r3Num;
    //Output final value from calculation
    NSString *raValue = [df3UP stringFromNumber:[NSNumber numberWithDouble:raNum]];
    raTF.text = raValue;
    NSString *rbValue = [df3UP stringFromNumber:[NSNumber numberWithDouble:rbNum]];
    rbTF.text = rbValue;
    NSString *rcValue = [df3UP stringFromNumber:[NSNumber numberWithDouble:rcNum]];
    rcTF.text = rcValue;
    }
    
    
    
    if(textfield.tag == 3 || textfield.tag == 4 || textfield.tag == 5){
    //Delta to WYE
    //store all the values from the textfields
    float raNum = [raTF.text floatValue];
    float rbNum = [rbTF.text floatValue];
    float rcNum = [rcTF.text floatValue];
    float r1Num, r2Num, r3Num;

    
    //Calculations
    double rDelta = raNum + rbNum + rcNum;
    r1Num = (rbNum * rcNum) / rDelta;
    r2Num = (raNum * rcNum) / rDelta;
    r3Num = (raNum *rbNum) / rDelta;
    
    //Output final value from calculation
    NSString *r1Value = [df3 stringFromNumber:[NSNumber numberWithDouble:r1Num]];
    r1TF.text = r1Value;
    NSString *r2Value = [df3 stringFromNumber:[NSNumber numberWithDouble:r2Num]];
    r2TF.text = r2Value;
    NSString *r3Value = [df3 stringFromNumber:[NSNumber numberWithDouble:r3Num]];
    r3TF.text = r3Value;
    
}


}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if (textField.tag == 0){
    r3TFset = 1;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
        return TRUE;
    }else if (textField.tag >= 1 || textField.tag <= 6 ){
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
        return TRUE;
    }
        return FALSE;
        
    }
    



- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    [self.view endEditing:YES];
    r3TFset = 0;
    return TRUE;
}




- (void)keyboardDidShow:(NSNotification *)notification
{

    // Assign new frame to your view
    if (r3TFset == 1){
    [self.view setFrame:CGRectMake(0,-110,self.view.frame.size.width,self.view.frame.size.height)]; //Move up frame if touched this certain textfield
    }else{
        [self.view setFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
    }
}





-(void)keyboardDidHide:(NSNotification *)notification
{
   [self.view setFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)]; // Move back after done typing
}

//End editing after you touch outside of the textfield
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
