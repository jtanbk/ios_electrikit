//
//  PowerView.h
//  Electrikit 0.1
//
//  Created by Jeremiah on 7/11/18.
//  Copyright (c) 2018 Jeremiah. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PowerView : UIViewController<UIPickerViewDelegate, UIPickerViewDataSource>

@property (weak, nonatomic) IBOutlet UITextField *voltagellTF;
@property (weak, nonatomic) IBOutlet UITextField *voltagelnTF;
@property (weak, nonatomic) IBOutlet UITextField *phaseTF;
@property (weak, nonatomic) IBOutlet UITextField *currentTF;
@property (weak, nonatomic) IBOutlet UITextField *powerfactTF;
@property (weak, nonatomic) IBOutlet UITextField *truepowerTF1;
@property (weak, nonatomic) IBOutlet UITextField *apparentpowerTF1;
@property (weak, nonatomic) IBOutlet UITextField *reactivepowerTF1;
@property (weak, nonatomic) IBOutlet UITextField *truepowerTF2;
@property (weak, nonatomic) IBOutlet UITextField *apparentpowerTF2;
@property (weak, nonatomic) IBOutlet UITextField *reactivepowerTF2;



@property (weak, nonatomic) IBOutlet UILabel *totaltitleLB;
@property (weak, nonatomic) IBOutlet UILabel *truepowerLB1;
@property (weak, nonatomic) IBOutlet UILabel *truepowerwLB1;
@property (weak, nonatomic) IBOutlet UILabel *apparentpowerLB1;
@property (weak, nonatomic) IBOutlet UILabel *apparentpowervaLB1;
@property (weak, nonatomic) IBOutlet UILabel *reactivepowerLB1;
@property (weak, nonatomic) IBOutlet UILabel *reactivepowervarLB1;
@property (weak, nonatomic) IBOutlet UILabel *powertitleLB;
@property (weak, nonatomic) IBOutlet UILabel *truepowerLB2;
@property (weak, nonatomic) IBOutlet UILabel *truepowerwLB2;
@property (weak, nonatomic) IBOutlet UILabel *apparentpowerLB2;
@property (weak, nonatomic) IBOutlet UILabel *apparentpowervaLB2;
@property (weak, nonatomic) IBOutlet UILabel *reactivepowerLB2;
@property (weak, nonatomic) IBOutlet UILabel *reactivepowervarLB2;
@property (weak, nonatomic) IBOutlet UILabel *llLB;
@property (weak, nonatomic) IBOutlet UILabel *voltagelnLB;
@property (weak, nonatomic) IBOutlet UILabel *voltagelnvLB;
@property (weak, nonatomic) IBOutlet UILabel *phaseLB;
@property (weak, nonatomic) IBOutlet UILabel *phasedegreeLB;
@property (weak, nonatomic) IBOutlet UILabel *powerfactLB1;
@property (weak, nonatomic) IBOutlet UILabel *powerfactLB2;

@property(strong, nonatomic) IBOutlet UIPickerView *powertrianglePK;



-(IBAction)pressCalc:(id)sender;





@end
