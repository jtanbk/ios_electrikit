//
//  MasterViewController.h
//  Electrikit 0.1
//
//  Created by Jeremiah on 6/26/18.
//  Copyright (c) 2018 Jeremiah. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MasterViewController : UITableViewController

@property NSMutableArray *Calculators;


@property (strong, nonatomic) IBOutlet UITableView *masterTV;

@end

