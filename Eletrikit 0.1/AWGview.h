//
//  AWGview.h
//  Electrikit 0.1
//
//  Created by Jeremiah on 6/27/18.
//  Copyright (c) 2018 Jeremiah. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CalculatorInfo.h"
#import "CalculatorDoc.h"

@interface AWGview : UIViewController<UIPickerViewDelegate, UIPickerViewDataSource>


@property (weak, nonatomic) IBOutlet UIPickerView *awgPK;
@property (weak, nonatomic) IBOutlet UITextField *inchesTF;
@property (weak, nonatomic) IBOutlet UITextField *inches2TF;
@property (weak, nonatomic) IBOutlet UITextField *mmTF;
@property (weak, nonatomic) IBOutlet UITextField *mm2TF;
@property (weak, nonatomic) IBOutlet UITextField *kcmTF;
@property (weak, nonatomic) IBOutlet UITextField *cuRTF;
@property (weak, nonatomic) IBOutlet UITextField *alRTF;
@property (weak, nonatomic) IBOutlet UITextField *curftTF;
@property (weak, nonatomic) IBOutlet UITextField *alrftTF;

@end






