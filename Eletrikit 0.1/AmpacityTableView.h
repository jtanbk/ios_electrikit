//
//  AmpacityTableView.h
//  Electrikit 0.1
//
//  Created by Jeremiah on 7/31/18.
//  Copyright (c) 2018 Jeremiah. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AmpacityTableView : UIViewController<UIApplicationDelegate, UIWebViewDelegate>



@property (weak, nonatomic) IBOutlet UIWebView *AmpacityTableWV;

@end



